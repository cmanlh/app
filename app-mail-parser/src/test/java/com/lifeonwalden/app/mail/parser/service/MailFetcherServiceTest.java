package com.lifeonwalden.app.mail.parser.service;

import com.lifeonwalden.app.mail.parser.bean.EmailInfo;
import com.lifeonwalden.app.mail.parser.service.impl.MailFetcherServiceImpl;
import com.lifeonwalden.app.util.character.JSON;
import com.sun.mail.imap.YoungerTerm;
import org.junit.Test;

import javax.mail.search.AndTerm;
import javax.mail.search.FromStringTerm;
import javax.mail.search.SearchTerm;
import java.util.List;

public class MailFetcherServiceTest {

    @Test
    public void fetch() throws Exception {
        MailFetcherServiceImpl service = new MailFetcherServiceImpl();
        service.setFileStorePath("E:\\IdeaProjects\\app\\app-mail-parser\\target\\")
                .setHost("172.16.46.4").setPassword("111111").setPort("25").setProtocol("imap").setUserName("lijinfang");
        service.afterPropertiesSet();
        SearchTerm[] fromList = {new FromStringTerm("lijinfang@orientsec.com.cn"), new YoungerTerm(60 * 60 * 10)};
        List<EmailInfo> list = service.fetch(new AndTerm(fromList));
        System.out.println(JSON.writeValueAsString(list));
    }
}
