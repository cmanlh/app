package com.lifeonwalden.app.mail.parser.service.impl;

import com.lifeonwalden.app.mail.parser.bean.AccountInfo;
import com.lifeonwalden.app.mail.parser.bean.AttachmentInfo;
import com.lifeonwalden.app.mail.parser.bean.EmailInfo;
import com.lifeonwalden.app.mail.parser.service.MailFetcherService;
import com.lifeonwalden.app.util.character.IdGenerator;
import com.lifeonwalden.app.util.logger.LoggerUtil;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.pop3.POP3Folder;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeUtility;
import javax.mail.internet.NewsAddress;
import javax.mail.search.SearchTerm;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class MailFetcherServiceImpl implements MailFetcherService, InitializingBean {
    private final static Logger logger = LoggerUtil.getLogger(MailFetcherServiceImpl.class);

    private String host;

    private String port;

    private String userName;

    private String password;

    private String protocol;

    private String folder = "INBOX";

    private String fileStorePath;

    private Session session;

    private static final String EMAIL_MULTIPART = "multipart/*";
    private static final String EMAIL_TEXT = "text/plain";
    private static final String EMAIL_HTML = "text/html";
    private static final String EMAIL_MESSAGE = "message/rfc822";

    @Override
    public List<EmailInfo> fetch(SearchTerm searchTerm) {
        LoggerUtil.debug(logger, "fetch", searchTerm);

        List<EmailInfo> emailList = new ArrayList<>();
        try (Store store = this.session.getStore()) {
            store.connect(this.host, this.userName, this.password);

            Folder folder = store.getFolder(this.folder);
            folder.open(Folder.READ_ONLY);

            Message[] msgs;
            if (null != searchTerm) {
                msgs = folder.search(searchTerm);
            } else {
                msgs = folder.getMessages();
            }
            if (null != msgs) {
                for (Message msg : msgs) {
                    try {
                        EmailInfo emailInfo = new EmailInfo();
                        if (folder instanceof POP3Folder) {
                            emailInfo.setUid(((POP3Folder) folder).getUID(msg));
                        } else if (folder instanceof IMAPFolder) {
                            emailInfo.setUid(String.valueOf(((IMAPFolder) folder).getUID(msg)));
                        }
                        fetchBasic(emailInfo, msg);
                        fetchAddress(emailInfo, msg);
                        fetchBody(emailInfo, msg);

                        emailList.add(emailInfo);
                    } catch (MessagingException e) {
                        logger.error("Failed to parse email", e);
                    }
                }
            }

            folder.close();
        } catch (NoSuchProviderException e) {
            logger.error("Not found email server", e);
        } catch (MessagingException e) {
            logger.error("Failed to parse email", e);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return emailList;
    }

    @Override
    public List<EmailInfo> fetch(SearchTerm searchTerm, List<String> skipUidList) {
        LoggerUtil.debug(logger, "fetch", searchTerm, skipUidList);

        List<EmailInfo> emailList = new ArrayList<>();
        Set<String> skipUidSet = skipUidList.stream().collect(Collectors.toSet());
        try (Store store = this.session.getStore()) {
            store.connect(this.host, this.userName, this.password);

            Folder folder = store.getFolder(this.folder);
            folder.open(Folder.READ_ONLY);

            Message[] msgs;
            if (null != searchTerm) {
                msgs = folder.search(searchTerm);
            } else {
                msgs = folder.getMessages();
            }
            if (null != msgs) {
                for (Message msg : msgs) {
                    try {
                        EmailInfo emailInfo = new EmailInfo();
                        if (folder instanceof POP3Folder) {
                            emailInfo.setUid(((POP3Folder) folder).getUID(msg));
                        } else if (folder instanceof IMAPFolder) {
                            emailInfo.setUid(String.valueOf(((IMAPFolder) folder).getUID(msg)));
                        }
                        fetchAddress(emailInfo, msg);
                        if (StringUtils.isNotEmpty(emailInfo.getFrom().getAddress())) {
                            if (skipUidSet.contains(emailInfo.getFrom().getAddress().concat(emailInfo.getUid()))) {
                                continue;
                            }
                        }
                        fetchBasic(emailInfo, msg);
                        fetchBody(emailInfo, msg);

                        emailList.add(emailInfo);
                    } catch (MessagingException e) {
                        logger.error("Failed to parse email", e);
                    }
                }
            }

            folder.close();
        } catch (NoSuchProviderException e) {
            logger.error("Not found email server", e);
        } catch (MessagingException e) {
            logger.error("Failed to parse email", e);
        } catch (IOException e) {
            logger.error("Failed to fetch email", e);
        }

        return emailList;
    }

    private void fetchBody(EmailInfo emailInfo, Part msg) throws MessagingException, IOException {
        if (msg.isMimeType(EMAIL_MULTIPART)) {
            Multipart multipart = (Multipart) msg.getContent();
            int count = multipart.getCount();
            for (int i = 0; i < count; i++) {
                BodyPart bodyPart = multipart.getBodyPart(i);
                if (bodyPart.isMimeType(EMAIL_MULTIPART)) {
                    fetchBody(emailInfo, bodyPart);
                } else if (bodyPart.isMimeType(EMAIL_TEXT)) {
                    String disposition = bodyPart.getDisposition();
                    if (Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
                        downloadAttachment(emailInfo, bodyPart);
                    } else {
                        Object text = bodyPart.getContent();
                        if (null != text) {
                            if (emailInfo.getText().isPresent()) {
                                emailInfo.setText(Optional.of(emailInfo.getText().get().concat((String) text)));
                            } else {
                                emailInfo.setText(Optional.of((String) text));
                            }
                        }
                    }
                } else if (bodyPart.isMimeType(EMAIL_HTML)) {
                    String disposition = bodyPart.getDisposition();
                    if (Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
                        downloadAttachment(emailInfo, bodyPart);
                    } else {
                        Object html = bodyPart.getContent();
                        if (null != html) {
                            if (emailInfo.getHtml().isPresent()) {
                                emailInfo.setHtml(Optional.of(emailInfo.getHtml().get().concat((String) html)));
                            } else {
                                emailInfo.setHtml(Optional.of((String) html));
                            }
                        }
                    }
                } else {
                    String disposition = bodyPart.getDisposition();
                    if (Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
                        downloadAttachment(emailInfo, bodyPart);
                    } else {
                        logger.warn("Unknown mime type.");
                    }
                }
            }
        } else if (msg.isMimeType(EMAIL_TEXT)) {
            Object text = msg.getContent();
            if (null != text) {
                if (emailInfo.getText().isPresent()) {
                    emailInfo.setText(Optional.of(emailInfo.getText().get().concat((String) text)));
                } else {
                    emailInfo.setText(Optional.of((String) text));
                }
            }
        } else if (msg.isMimeType(EMAIL_HTML)) {
            Object html = msg.getContent();
            if (null != html) {
                if (emailInfo.getHtml().isPresent()) {
                    emailInfo.setHtml(Optional.of(emailInfo.getHtml().get().concat((String) html)));
                } else {
                    emailInfo.setHtml(Optional.of((String) html));
                }
            }
        } else if (msg.isMimeType(EMAIL_MESSAGE)) {
            fetchBody(emailInfo, (Part) msg.getContent());
        }
    }

    private void fetchBasic(EmailInfo emailInfo, Message msg) throws MessagingException {
        emailInfo.setSubject(msg.getSubject());
        if (null != msg.getReceivedDate()) {
            emailInfo.setReceiveDate(Optional.of(msg.getReceivedDate()));
        }
        if (null != msg.getSentDate()) {
            emailInfo.setSentDate(Optional.of(msg.getSentDate()));
        }
    }

    private void fetchAddress(EmailInfo emailInfo, Message msg) throws MessagingException {
        Address[] froms = msg.getFrom();
        if (null != froms && froms.length > 0) {
            Optional<AccountInfo> from = processAddress(froms[0]);
            if (from.isPresent()) {
                emailInfo.setFrom(from.get());
            }
        }

        Address[] toAddressList = msg.getRecipients(Message.RecipientType.TO);
        if (null != toAddressList && toAddressList.length > 0) {
            List<AccountInfo> toList = new ArrayList<>();
            for (Address toAddress : toAddressList) {
                Optional<AccountInfo> to = processAddress(toAddress);
                if (to.isPresent()) {
                    toList.add(to.get());
                }
            }
            if (!toList.isEmpty()) {
                emailInfo.setTo(Optional.of(toList));
            }
        }

        Address[] ccAddressList = msg.getRecipients(Message.RecipientType.CC);
        if (null != ccAddressList && ccAddressList.length > 0) {
            List<AccountInfo> ccList = new ArrayList<>();
            for (Address ccAddress : ccAddressList) {
                Optional<AccountInfo> cc = processAddress(ccAddress);
                if (cc.isPresent()) {
                    ccList.add(cc.get());
                }
            }
            if (!ccList.isEmpty()) {
                emailInfo.setCc(Optional.of(ccList));
            }
        }

        Address[] bccAddressList = msg.getRecipients(Message.RecipientType.BCC);
        if (null != bccAddressList && bccAddressList.length > 0) {
            List<AccountInfo> bccList = new ArrayList<>();
            for (Address bccAddress : bccAddressList) {
                Optional<AccountInfo> bcc = processAddress(bccAddress);
                if (bcc.isPresent()) {
                    bccList.add(bcc.get());
                }
            }
            if (!bccList.isEmpty()) {
                emailInfo.setBcc(Optional.of(bccList));
            }
        }
    }

    private void downloadAttachment(EmailInfo emailInfo, BodyPart bodyPart) throws MessagingException, IOException {
        String fileName = bodyPart.getFileName();
        if (null != fileName) {
            AttachmentInfo attachmentInfo = new AttachmentInfo();
            attachmentInfo.setName(MimeUtility.decodeText(fileName)).setPath(fileStorePath.concat(IdGenerator.getId()));

            byte[] buffer = new byte[1024];
            int length = 0;
            try (InputStream is = bodyPart.getInputStream(); FileOutputStream fos = new FileOutputStream(attachmentInfo.getPath())) {
                while ((length = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, length);
                }
            }

            if (emailInfo.getEmailAttachments().isPresent()) {
                emailInfo.getEmailAttachments().get().add(attachmentInfo);
            } else {
                List<AttachmentInfo> attachmentInfoList = new ArrayList<>();
                attachmentInfoList.add(attachmentInfo);
                emailInfo.setEmailAttachments(Optional.of(attachmentInfoList));
            }
        }

    }

    private Optional<AccountInfo> processAddress(Address address) {
        if ("RFC822".equalsIgnoreCase(address.getType())) {
            InternetAddress internetAddress = (InternetAddress) address;
            AccountInfo accountInfo = new AccountInfo();
            accountInfo.setAddress(internetAddress.getAddress()).setName(null == internetAddress.getPersonal() ? Optional.empty() : Optional.of(internetAddress.getPersonal()));

            return Optional.of(accountInfo);
        } else if ("RFC1036".equalsIgnoreCase(address.getType())) {
            NewsAddress newsAddress = (NewsAddress) address;
            AccountInfo accountInfo = new AccountInfo();
            accountInfo.setAddress(newsAddress.getHost()).setName(null == newsAddress.getNewsgroup() ? Optional.empty() : Optional.of(newsAddress.getNewsgroup()));

            return Optional.of(accountInfo);
        }

        return Optional.empty();
    }

    public String getHost() {
        return host;
    }

    public MailFetcherServiceImpl setHost(String host) {
        this.host = host;

        return this;
    }

    public String getPort() {
        return port;
    }

    public MailFetcherServiceImpl setPort(String port) {
        this.port = port;

        return this;
    }

    public String getUserName() {
        return userName;
    }

    public MailFetcherServiceImpl setUserName(String userName) {
        this.userName = userName;

        return this;
    }

    public String getPassword() {
        return password;
    }

    public MailFetcherServiceImpl setPassword(String password) {
        this.password = password;

        return this;
    }

    public String getProtocol() {
        return protocol;
    }

    public MailFetcherServiceImpl setProtocol(String protocol) {
        this.protocol = protocol;

        return this;
    }

    public String getFolder() {
        return folder;
    }

    public MailFetcherServiceImpl setFolder(String folder) {
        this.folder = folder;

        return this;
    }

    public String getFileStorePath() {
        return fileStorePath;
    }

    public MailFetcherServiceImpl setFileStorePath(String fileStorePath) {
        this.fileStorePath = fileStorePath;

        return this;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", this.host);
        props.setProperty("mail.store.protocol", this.protocol);
        props.setProperty("mail.smtp.port", this.port);
        this.session = Session.getDefaultInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        });
    }
}
