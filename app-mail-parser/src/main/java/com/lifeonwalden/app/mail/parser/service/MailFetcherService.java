package com.lifeonwalden.app.mail.parser.service;

import com.lifeonwalden.app.mail.parser.bean.EmailInfo;

import javax.mail.search.SearchTerm;
import java.util.List;

public interface MailFetcherService {
    List<EmailInfo> fetch(SearchTerm searchTerm);

    List<EmailInfo> fetch(SearchTerm searchTerm, List<String> skipUidList);
}