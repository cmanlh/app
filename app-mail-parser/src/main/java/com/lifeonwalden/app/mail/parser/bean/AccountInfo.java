package com.lifeonwalden.app.mail.parser.bean;

import java.util.Optional;

public class AccountInfo {
    private Optional<String> name;
    private String address;

    public Optional<String> getName() {
        return name;
    }

    public AccountInfo setName(Optional<String> name) {
        this.name = name;

        return this;
    }

    public String getAddress() {
        return address;
    }

    public AccountInfo setAddress(String address) {
        this.address = address;

        return this;
    }
}
