package com.lifeonwalden.app.mail.parser.bean;

public class AttachmentInfo {
    private String name;
    private String path;

    public String getName() {
        return name;
    }

    public AttachmentInfo setName(String name) {
        this.name = name;

        return this;
    }

    public String getPath() {
        return path;
    }

    public AttachmentInfo setPath(String path) {
        this.path = path;

        return this;
    }
}
