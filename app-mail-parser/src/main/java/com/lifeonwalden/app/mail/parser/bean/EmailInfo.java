package com.lifeonwalden.app.mail.parser.bean;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class EmailInfo {
    private String uid;
    private String subject;
    private AccountInfo from;
    private Optional<List<AccountInfo>> to = Optional.empty();
    private Optional<List<AccountInfo>> cc = Optional.empty();
    private Optional<List<AccountInfo>> bcc = Optional.empty();
    private Optional<Date> sentDate = Optional.empty();
    private Optional<Date> receiveDate = Optional.empty();
    private Optional<List<AttachmentInfo>> emailAttachments = Optional.empty();
    private Optional<String> text = Optional.empty();
    private Optional<String> html = Optional.empty();

    public String getUid() {
        return uid;
    }

    public EmailInfo setUid(String uid) {
        this.uid = uid;

        return this;
    }

    public String getSubject() {
        return subject;
    }

    public EmailInfo setSubject(String subject) {
        this.subject = subject;

        return this;
    }

    public AccountInfo getFrom() {
        return from;
    }

    public EmailInfo setFrom(AccountInfo from) {
        this.from = from;

        return this;
    }

    public Optional<List<AccountInfo>> getTo() {
        return to;
    }

    public EmailInfo setTo(Optional<List<AccountInfo>> to) {
        this.to = to;

        return this;
    }

    public Optional<List<AccountInfo>> getCc() {
        return cc;
    }

    public EmailInfo setCc(Optional<List<AccountInfo>> cc) {
        this.cc = cc;

        return this;
    }

    public Optional<List<AccountInfo>> getBcc() {
        return bcc;
    }

    public EmailInfo setBcc(Optional<List<AccountInfo>> bcc) {
        this.bcc = bcc;

        return this;
    }

    public Optional<Date> getSentDate() {
        return sentDate;
    }

    public EmailInfo setSentDate(Optional<Date> sentDate) {
        this.sentDate = sentDate;

        return this;
    }

    public Optional<Date> getReceiveDate() {
        return receiveDate;
    }

    public EmailInfo setReceiveDate(Optional<Date> receiveDate) {
        this.receiveDate = receiveDate;

        return this;
    }

    public Optional<List<AttachmentInfo>> getEmailAttachments() {
        return emailAttachments;
    }

    public EmailInfo setEmailAttachments(Optional<List<AttachmentInfo>> emailAttachments) {
        this.emailAttachments = emailAttachments;

        return this;
    }

    public Optional<String> getText() {
        return text;
    }

    public EmailInfo setText(Optional<String> text) {
        this.text = text;

        return this;
    }

    public Optional<String> getHtml() {
        return html;
    }

    public EmailInfo setHtml(Optional<String> html) {
        this.html = html;

        return this;
    }
}
