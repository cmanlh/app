$JqcLoader.registerModule($JqcLoader.newModule('com.jquery', LIB_ROOT_PATH).registerComponents(['jquery', 'keycode', 'version']))
    .registerModule($JqcLoader.newModule('com.lifeonwalden.jqc', LIB_ROOT_PATH)
        .registerComponents(['baseElement', 'blocker', 'cache', 'dateUtil', 'dialog', 'draggable', 'format', 'inputNumber', 'lang'])
        .registerComponents(['loader', 'location', 'pinyin', 'resizeable', 'selectbox', 'slide', 'uniqueKey', 'slide', 'uniqueKey'])
        .registerComponents(['valHooks', 'zindex'])
        .registerComponents(['toolkit'])
        .registerComponents(['tooltips'])
        .registerComponents(['menuTree'])
        .registerComponents(['notification', 'tag', 'calendar', 'notify'])
        .registerComponents(['contextmenu','layoutHelper'])
        .registerComponents(['loading'])
        .registerComponents(['confirm'])
        .registerComponents(['event'])
        .registerComponents(['asyncSelect'])
        .registerComponents(['timepicker'])
        .registerComponents(['select'])
        .registerComponents(['icon'])
        .registerComponents(['upload'])
        .registerComponents(['multipleUpload'])
        .registerComponents(['nav'])    // 按需加载
        .registerComponents(['formToolBar', 'formUtil', 'jqcElementUtil', 'datetimepicker', 'tip', 'msg', 'tab'])
        .registerComponents(['echarts']) //图表
        .registerComponents(['gcharts']) //图表
        .registerComponents(['bpmn']) // bpmn2.0
        .registerComponents(['timeline']) //时间线
        .registerComponents(['jsoneditor']) //json编辑器图表
        .registerComponents(['editor']) //富文本编辑器
        .registerComponents(['js-xlsx']) //解析表格
        .registerComponents(['importExcel']) //导入表格
        .registerComponents(['checkbox'])
        .registerComponents(['textarea'])
        .registerComponents(['input'])
        .registerComponents(['picker'])
        .registerComponents(['charUtil'])
        .registerComponents(['crypto'])
        .registerComponents(['apisBox']));
// 字符串方法拓展
if (!String.prototype.padStart) {
    String.prototype.padStart = function (len, fillStr) {
        var arr = this.split('');
        for( var i = this.length; i< len; i++) {
            arr.unshift(fillStr);
        }
        return arr.join('');
    }
}
if (!String.prototype.padEnd) {
    String.prototype.padEnd = function (len, fillStr) {
        var arr = this.split('');
        for( var i = this.length; i< len; i++) {
            arr.push(fillStr);
        }
        return arr.join('');
    }
}
$JqcLoader.importComponents('com.jquery', ['jquery', 'keycode', 'version'])
    .importComponents('com.lifeonwalden.jqc', ['input', 'select', 'asyncSelect', 'confirm', 'checkbox', 'textarea', 'event', 'menuTree', 'formUtil', 'jqcElementUtil', 'msg', 'tab', 'dialog', 'formToolBar', 'contextmenu', 'toolkit', 'tooltips', 'loading','layoutHelper', 'notification', 'tag', 'calendar', 'icon', 'upload', 'multipleUpload', 'notify'])
    // dx组件
    .importScript(LIB_ROOT_PATH.concat('com/devexpress/jszip.min.js'))
    .importScript(LIB_ROOT_PATH.concat('com/devexpress/dx.web.js'))
    .importScript(LIB_ROOT_PATH.concat('com/devexpress/dx.messages.zh.js'))
    .importCss(LIB_ROOT_PATH.concat('com/devexpress/css/dx.common.css'))
    .importCss(LIB_ROOT_PATH.concat('com/devexpress/css/dx.light.css'))
    // 全局配置
    .importCss(LIB_ROOT_PATH.concat('com/lifeonwalden/app/css/app.css'))
    .importCss(LIB_ROOT_PATH.concat('com/lifeonwalden/app/css/dx.custom.css'))
    .execute(function() {
        DevExpress.localization.locale('zh');
        const T = $.jqcToolkit;
        const pinyinParser = new $.jqcPinyin();
        const Loading = new $.jqcLoading();
        const debounce = $.jqcToolkit.debounce; // 防抖
        const queue = $.jqcToolkit.queue;   // 队列执行
        const $add = $.jqcToolkit.add;  // 加法
        const $sub = $.jqcToolkit.sub;  // 减法
        const $mul = $.jqcToolkit.mul;  // 乘法
        const $div = $.jqcToolkit.div;  // 除法
        const COMP_LIB_PATH = 'com.lifeonwalden.jqc';
        function getQueryString(name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return unescape(r[2]);
            }
            return null;
        }

        $.datetimepicker.setLocale('zh');
        // 模板文件功能区域对应的类名
        const templateClassNameMap = {
            conditionHtmlClassName: 'toolbar-left', //toolbar左侧
            controlHtmlClassName: 'toolbar-right', //toobar右侧
            contentHtmlClassName: 'content' //自定义内容
        };
        // dxDataGrid默认配置
        const dxDataGridDefaultConfig = {        
            exportProxyClassName: 'export',
            searchProxyClassName: 'search',
            dataSource: [],
            allowColumnResizing: true,
            height: window.innerHeight - 170,
            filterRow: {
                visible: true,
            },
            headerFilter: {
                visible: true,
            },
            hoverStateEnabled: true,
            rowAlternationEnabled: true,
            grouping: {
                expandMode: 'rowClick'
            },
            columnAutoWidth: true,
            scrolling: {
                mode: 'infinite',
                // showScrollbar: 'always'
            },
            paging: {
                pageSize: 100000
            },
            showRowLines: true,
            showBorders: true,
            columns: [{
                caption: '序号',
                fixed: true,
                width: 70,
                cssClass: 'bgf5f6fa',
                alignment: 'center',
                cellTemplate: function(box, data) {
                    var index = data.rowIndex + 1;
                    var total = data.component.option('dataSource').length;
                    box.text(index);
                    box.attr('title', `第${index}条/共${total}条`);
                },
                allowExporting: false
            }],
            loadPanel: {
                enabled: false
            },
            onContentReady: function () {
                jqcEvent.emit('filled.dxDataGrid');
            }
        };
    
        const noIndexConfig = {
            templateClassNameMap,
            dxDataGridDefaultConfig: Object.assign({}, dxDataGridDefaultConfig, {
                columns: []
            })
        }
        const globalConfig = {
            templateClassNameMap,
            dxDataGridDefaultConfig
        };
        $.getGlobalConfig = function(hideIndex) {
            if (hideIndex) {
                return noIndexConfig;
            }
            return globalConfig || {};
        };
        // 需要编辑预处理的dxDataGrid默认配置
        const dxDataGridDefaultConfigWithEditor = {        
            exportProxyClassName: 'export',
            searchProxyClassName: 'search',
            dataSource: [],
            allowColumnResizing: true,
            height: window.innerHeight - 170,
            filterRow: {
                visible: true,
            },
            headerFilter: {
                visible: true,
            },
            hoverStateEnabled: true,
            rowAlternationEnabled: true,
            grouping: {
                expandMode: 'rowClick'
            },
            columnAutoWidth: true,
            scrolling: {
                mode: 'infinite',
                // showScrollbar: 'always'
            },
            paging: {
                pageSize: 100000
            },
            showRowLines: true,
            showBorders: true,
            columns: [{
                caption: '序号',
                fixed: true,
                width: 70,
                cssClass: 'bgf5f6fa',
                alignment: 'center',
                cellTemplate: function(box, data) {
                    var index = data.rowIndex + 1;
                    var total = data.component.option('dataSource').length;
                    box.text(index);
                    box.attr('title', `第${index}条/共${total}条`);
                },
                allowExporting: false
            }],
            loadPanel: {
                enabled: false
            },
            onContentReady: function (e) {
                jqcEvent.emit('filled.dxDataGrid');
                 // 拷贝原数据
                 e.component.originDataSource = e.component.originDataSource ? e.component.originDataSource : JSON.parse(JSON.stringify(e.component._options.dataSource));
                 // 当前实例扩展存放被修改数据
                 e.component.changedData = e.component.changedData ? e.component.changedData : new Set();
                 // 当前实例扩展方法 提供获取被修改数据
                 e.component.getChangedData = function () {
                     return Array.from(e.component.changedData).length > 0 ? Array.from(e.component.changedData) : null;
                 }
            },
            // 编辑row 之前
            onEditorPreparing: function (e) {
              // 修改之前默认操作状态为 0 
              let onEditorPreparingExtension = e.component._options.onEditorPreparingExtension;
              if(onEditorPreparingExtension && typeof onEditorPreparingExtension === 'function') {
                onEditorPreparingExtension(e);
              }
            },
            // 编辑的row
            onRowUpdated: function (e) {
                e.key.opType = 2;
                e.component.changedData.add(e.key);
                let onRowUpdatedExtension = e.component._options.onRowUpdatedExtension;
                if(onRowUpdatedExtension && typeof onRowUpdatedExtension === 'function') {
                    onRowUpdatedExtension(e);
                }
            },
            // 插入新row
            onRowInserted: function (e){
                e.key.opType = 1;
                // 插入保存数据
                e.component.changedData.add(e.key);
                let onRowInsertedExtension = e.component._options.onRowInsertedExtension;
                if(onRowInsertedExtension && typeof onRowInsertedExtension === 'function') {
                    onRowInsertedExtension(e);
                }
            },
            // 移除row
            onRowRemoved: function (e) {
                e.key.opType = 3;
                // 删除时存储数据
                var isNewData = true;
                e.component.originDataSource.forEach(val => {
                    // 不是新插入的数据
                    if(val.id === e.key.id) { 
                        e.component.changedData.add(e.key);
                        isNewData = false;
                    }
                })
                // 删除新插入数据 从changedData去除
                e.component.changedData.forEach(val => {
                    if(isNewData && val.id === e.key.id) {
                        e.component.changedData.delete(e.key);
                    }
                })
                
                let onRowRemovedExtension = e.component._options.onRowRemovedExtension;
                if(onRowRemovedExtension && typeof onRowRemovedExtension === 'function') {
                    onRowRemovedExtension(e);
                }
            },
            // row 渲染完毕
            onRowPrepared: function (e){
                let onRowPreparedExtension = e.component._options.onRowPreparedExtension;
                if(onRowPreparedExtension && typeof onRowPreparedExtension === 'function') {
                    onRowPreparedExtension(e);
                }
            }
        };
        const noIndexConfigWithEditor = {
            templateClassNameMap,
            dxDataGridDefaultConfig: Object.assign({}, dxDataGridDefaultConfigWithEditor, {
                columns: []
            })
        }
        const globalConfigWithEditor = {
            templateClassNameMap,
            dxDataGridDefaultConfigWithEditor
        };
        $.getGlobalConfigWithEditor = function(hideIndex) {
            if (hideIndex) {
                return noIndexConfigWithEditor;
            }
            // dxDataGridDefaultConfigWithEditor.onEditorPreparingExtension = null;
            // dxDataGridDefaultConfigWithEditor.onRowUpdatedExtension = null;
            // dxDataGridDefaultConfigWithEditor.onRowInsertedExtension = null;
            // dxDataGridDefaultConfigWithEditor.onRowRemovedExtension = null;
            // dxDataGridDefaultConfigWithEditor.onRowPreparedExtension = null;
            return globalConfigWithEditor || {};
        };
        // end 编辑预处理 dxDataGrid默认配置
        // 判断是否是枚举对象
        $.isEnum = function (_enum) {
            if (!_enum) {
                return false;
            }
            if (typeof Enum !== 'function') {
                return false;
            }
            return _enum instanceof Enum;
        }
        // download
        $.download = function (url, tip) {
            var $iframe = $('<iframe src="' + url + '" style="display:none"/>');
            $('body').append($iframe);
            if(tip) {
                Loading.show('下载已开始，请耐心等待！');
                let t = setTimeout(() => {
                    Loading.hide();
                    clearTimeout(t);
                }, 2000);
            }
            
            
            var timer;
            $iframe.on({
                'load': function () {
                    var $doc = $(this.contentDocument);
                    var res = $doc.find('body').text() || doc.find('head').text();
                    var msg;
                    try {
                        msg = JSON.parse($.trim(res)).msg;
                    } catch (err) {
                        
                    }
                    if (msg) {
                        $.tip('error', '下载失败', msg);
                    }
                    clearTimeout(timer);
                    $iframe.remove();
                }
            });
            timer = setTimeout(() => {
                $iframe && $iframe.remove();
            }, 45000);
        }
        // tip
        $.tip = function (type, title, content, duration) {
            $.jqcNotification({
                type,
                title,
                content,
                duration
            });
        }
        // mixin
        const mixinCache = new Map();
        window.MIXIN = {
            add: function (key, value) {
                if (mixinCache.has(key.toString())) {
                    return;
                }
                mixinCache.set(key.toString(), value);
            },
            get: function (key) {
                var _key = key.toString();
                if (!mixinCache.has(_key)) {
                    throw new Error('请确定mixin对象 '+_key+ ' 是否存在！');
                }
                return (mixinCache.get(key.toString()) || {});
            }
        };
        // dxDataGrid数据格式化
        // 时间格式化
        $.timeFormat = function (format) {
            return function (value) {
                return $.jqcDateUtil.format(+value, format);
            }
        }
        // 保留小数
        $.numberFormat = function (num=2, postfix='', prefix='') {
            return function (value) {
                var _temp = typeof value === 'number' ? value : +value;
                return prefix + _temp.toFixed(num) + postfix;
            }
        }
        // 时间戳转时间对象
        $.timestamp2date = function (name) {
            return function (rowData) {
                return rowData[name] ? new Date(+(rowData[name])) : '';
            }
        }
        // 计算年份
        function calculateYearFromNowByString(fx) {
            if (typeof fx === 'undefined') {
                return;
            }
            var now = new Date().getFullYear();
            fx = String(fx).toLowerCase().replace(' ', '');
            if (fx === '' || fx === 'n') {
                return now;
            }
            if (fx.indexOf('n') === 0) {
                fx = +(fx.replace('n', ''));
                return now + fx;
            }
            if (!isNaN(fx)) {
                return +fx;
            }
        }
        /**
         * 金额格式化
         * @param {number} num              // 需要格式化的数值
         * @param {int} precision           // 需要保留的小数位数
         * @param {string} prefix           // 前缀
         * @param {string} suffix           // 后缀
         * @param {number|boolean} isRound   // 是否四舍五入，默认否
         */
        $.currencyFormatter = function (num, precision, prefix='', suffix='', isRound) {
            if (num == undefined) {
                return '';
            }
            num = num.toString().replace(/\$|\,/g,'');
            if(isNaN(num))
                num = "0";
            if (precision === undefined || precision === null) {
                var _num = num.split('.');
                precision = _num[1] === undefined ? 0 : _num[1].length;
            }
            var sign = (num == (num = Math.abs(num)));
            var scale = Math.pow(10, precision);
            var scaleNum = $mul(num, scale);
            if (isRound) {
                num = Math.round(scaleNum);
            } else {
                num = Math.floor(scaleNum);
            }
            var cents = num%scale;
            num = Math.floor($div(num, scale)).toString();
            if(precision > 0) {
                cents = '.' + cents.toString().padStart(precision, '0');
            } else {
                cents = '';
            }
            for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                num = num.substring(0,num.length-(4*i+3))+','+
            num.substring(num.length-(4*i+3));
            return prefix + (((sign)?'':'-') + num + cents) + suffix;
        }
        $.numberFormatter = function (num, precision, prefix='', suffix='', isRound,quantile,symbol) {
            if (num == undefined) {
                return '';
            }
            if (suffix == '100%') {
                suffix = '%'
                num = +num*100
            }
            num = num.toString().replace(/\$|\,/g,'');
            if(isNaN(num))
                num = "0";
            if (precision === undefined || precision === null) {
                var _num = num.split('.');
                precision = _num[1] === undefined ? 0 : _num[1].length;
            }
            var sign = (num == (num = Math.abs(num)));
            var scale = Math.pow(10, precision);
            var scaleNum = $mul(num, scale);
            if (isRound) {
                num = Math.round(scaleNum);
            } else {
                num = Math.floor(scaleNum);
            }
            var cents = num%scale;
            num = Math.floor($div(num, scale)).toString();
            if(precision > 0) {
                cents = '.' + cents.toString().padStart(precision, '0');
            } else {
                cents = '';
            }
            num = parseInt(num) + '';
            var lastNum = prefix + (((sign)?'':'-') + num + cents) + suffix
            if (quantile) {
                symbol = symbol || ','
                var fofInfoKey = Math.abs(+lastNum)
                if (fofInfoKey > +quantile) {
                    var strNum = lastNum
                    if (strNum.indexOf('.') != -1) {
                        strNum = strNum.split('.')
                        strNum[0] = strNum[0].replace(/(\d)(?=(\d{3})+(?!\d))/g,'$1'+symbol)
                        return strNum.join('.')
                    } else  {
                        return strNum.replace(/(\d)(?=(\d{3})+(?!\d))/g,'$1'+symbol)
                    }
                }
            }
            return lastNum
            // return prefix + (((sign)?'':'-') + num + cents) + suffix;
        }
        /* *******************jQuery对象封装********************* */
        $.prototype.inputSelect = $.prototype.select;
        
        $.fn.extend({
            /**
             * params.data  数据
             * params.adapter   适配 value/label
             * params.defaultVal    默认值
             */
            select: function (data, defaultVal, params) {
                var $el = this;
                var _el = this[0];
                if (!_el || (_el.nodeName != 'INPUT')) {
                    return;
                }
                if ($el.attr('off') != undefined) {
                    return;
                }
                var _data;
                var adapter = {};
                var dataType = data.constructor.name;
                if ($.isEnum(data)) {
                    dataType = 'Enum';
                }
                switch (dataType) {
                    case 'Enum':
                        _data = data.values();
                        adapter = {
                            label: 'desc'
                        };
                        break;
                    case 'Array':
                        _data = data;
                        adapter = undefined;
                        break;
                    case 'Object':
                        _data = data.data;
                        adapter = data.adapter || {};
                        break;
                    default:
                        break;
                }
                var mode = this.attr('multiple');
                var config = {
                    el: $el,
                    data: _data,
                    mode: mode !== undefined ? 'multiple' : 'single',
                    adapter,
                    defaultValue: defaultVal
                };
                if (params) {
                    Object.assign(config, params);
                }
                new $.jqcSelect(config);
                return this;
            },
            // 带有搜索的下拉框 $selectSearch
            selectSearch: function (data, defaultVal, params) {
                var $el = this;
                var _el = this[0];
                var _data = {};
                var _defaultVal = defaultVal;
                var _updateDataSource = undefined;
                if (!_el || (_el.nodeName != 'INPUT')) {
                    return;
                }
                if ($el.attr('off') != undefined) {
                    return;
                }
                if (this.attr('defaultvalue') != undefined) {
                    _defaultVal = this.attr('defaultvalue');
                }
                var dataType = data.constructor.name;
                if ($.isEnum(data)) {
                    dataType = 'Enum';
                }
                switch (dataType) {
                    case 'Enum':
                        _data.data = data.values();
                        _data.adapter = {
                            value: 'value',
                            label: 'desc',
                            filter: 'value',
                            pinyinFilter: 'desc'
                        };
                        break;
                    case 'Array':
                        _data.data = data;
                        _data.adapter = {
                            value: 'value',
                            label: 'label',
                            filter: 'value',
                            pinyinFilter: 'label'
                        };
                        break;
                    case 'Object':
                        _data = data;
                        break;
                    default:
                        break;
                }
                var all;
                if (this.attr('ext') == '*' || _defaultVal == '*') {
                    var _value = _data.adapter.value;
                    var _label = _data.adapter.label;
                    var _filter = _data.adapter.filter;
                    var _pinyinFilter = _data.adapter.pinyinFilter;
                    if (typeof _label === 'function') {
                        var _temp = _label.bind();
                        _label = _data.adapter.pinyinFilter;
                        _data.adapter.label = function (item) {
                            if (item[_value] === '*') {
                                return '全部';
                            }
                            return  _temp(item);
                        }
                    }
                    all = {
                        [_value]: '*',
                        [_label]: '全部',
                    };
                    _filter && (all[_filter] = '*');
                    _pinyinFilter && (all[_pinyinFilter] = '全部');
                    _data.data = [all].concat(_data.data);
                }
                // 新增更新jqcSelectBox数据方法，refreshSourceName：缓存池中数据对应键 但不影响之前通过refreshApi强制更新数据方法
                if (data.refreshSourceName && typeof data.refreshSourceName === 'string') {
                    // 如果未传递缓存池 默认$.CACHE
                    data.DataPool ? '' : data.DataPool = $.CACHE
                    _updateDataSource = function (callback) {
                        if (data.DataPool && data.DataPool.get && typeof data.DataPool.get === 'function') {
                            // 根据键名拉取最新数据
                            let source = data.DataPool.get(data.refreshSourceName)
                            if (!$.isArray(source)) {
                                throw new Error('the data must be Array!');
                            }
                            callback(source)
                        } else {
                            throw new Error('DataPool is not correct!');
                        }

                    }
                }
                if (data.refreshApi != undefined) {
                    if (typeof data.refreshApi == 'string') {
                        _updateDataSource = function (callback) {
                            $.ajax(data.refreshApi).then(res => {
                                var result = res.result || [];
                                callback(result);
                            });
                        }
                    } else if (typeof data.refreshApi == 'function') {
                        _updateDataSource = data.refreshApi;
                    }
                }
                _el.jqcSelectBox && _el.jqcSelectBox.destroy();
                var config = {
                    optionData: _data,
                    defaultVal: _defaultVal,
                    dataName: $.jqcToolkit.uuid(20),
                    autoDisplay: true,
                    supportFuzzyMatch: true,
                    supportPinYin: true,
                    pinyinParser: pinyinParser,
                    withSearch: true,
                    element: $el,
                    forceUpdate: (_updateDataSource || data.refreshSourceName) ? true : false,
                    updateDataSource: _updateDataSource ? function (callback) {
                        _updateDataSource(function (data) {
                            if (all) {
                                data = [all].concat(data);
                            }
                            callback(data);
                        });
                    } : false,
                    onSelect: function (data) {
                        $el.trigger('change', data);
                    }
                };
                if (params) {
                    Object.assign(config, params);
                }
                _el.jqcSelectBox = new $.jqcSelectBox(config);
                return this;
            },
            // 销毁组件 $destroy
            destroySelectBox: function () {
                $.each(this, function (index, el) {
                    if (el.jqcSelectBox) {
                        el.jqcSelectBox.destroy();
                        el.jqcSelectBox = undefined;
                    }
                    if ($(el).data('jqcSelect') && $(el).data('jqcSelect').destroy) {
                        $(el).data('jqcSelect').destroy();
                    }
                    if ($(el).data('xdsoft_datetimepicker')) {
                        $(el).datetimepicker('destroy');
                    }
                    if ($(el).data('jqcInput') && $(el).data('jqcInput').destroy) {
                        $(el).data('jqcInput').destroy();
                    }
                });
                return this;
            },
            // 文件上传
            $upload: function (config, defaultValue) {
                $.each(this, function (index, el) {
                   var $this = $(el);
                   var _width = $this.outerWidth();
                   var _disabled = $this.prop('disabled');
                   var _placeholder = $this.prop('placeholder');
                   var tipId = 'tipId' + $.jqcToolkit.uuid(8);
                   config && config.deleteTipFor ? '' :  $this.attr('tip:for', tipId);
                   var $upload = $('<div>').css('width', _width).addClass(tipId);
                   $this.prop('hidden', true).before($upload);
                   $this.off('setValue');
                   var baseUrl = (config && config.baseUrl) ? config.baseUrl : './';
                   var accept = $this.attr('accept') ? $this.attr('accept').split(',') : [];
                   var maxSize = $this.attr('maxsize') || 0;
                   var uploadConfig = Object.assign({
                        el: $upload,
                        url: baseUrl + 'file/upload',
                        maxSize,
                        accept,
                        width: _width,
                        disabled: _disabled,
                        name: 'file',
                        placeholder: _placeholder || '请选择文件',
                        autoUpload: true,
                        downloadFormat: function (id) {
                            return baseUrl + 'file/download/' + id;
                        },
                        onRemove: function (id, success) {
                            el.hasBlur = true;
                            el.fileName = undefined;
                            $this.val('').trigger('change', '').trigger('check');
                            success();
                        },
                        beforeUpload: function (next, data, files) {
                            Loading.show();
                            el.fileName = files[0].name;
                            next();
                        },
                        afterUpload: function (res, success, error) {
                            Loading.hide();
                            if (res.code == 0) {
                                var backendId = res.result.backendId;
                                // $this.val(backendId);
                                success({
                                    [res.result.frontendId]: backendId
                                });
                                el.value = backendId;
                                $this.trigger('change', backendId).trigger('check');
                            } else if (res.code == 1) {
                                error();
                            }
                        },
                        error: function (data, next) {
                            Loading.hide();
                            el.fileName = undefined;
                            next();
                        }
                    }, (config || {}));
                    $this.addClass('valHooks');
                    el.upload = new $.jqcUpload(uploadConfig);
                    $this.on('setValue', function (e, id) {
                        if (id === '') {
                            this.upload.clear();
                            this.value = '';
                            return;
                        }
                        if (id) {
                            this.upload.showBack(id);
                            this.value = id;
                            $(this).trigger('change', id).trigger('check');
                        }
                    });
                    if (defaultValue !== undefined) {
                        el.upload.showBack(defaultValue);
                        $this.val(defaultValue);
                    }
                    Object.defineProperty(el, 'disabled', {
                        set: function (value) {
                            this.__disabled__ = value;
                            this.upload.disabled = value;
                        },
                        get: function () {
                            return this.__disabled__ || false;
                        }
                    })
                });
                return this;
            },
            // 绑定防抖事件
            $debounce: function (actionName, actionFunc, wait, immediate) {
                wait = wait || 1000;
                immediate = immediate === undefined ? true : immediate;
                this.on(actionName, debounce(actionFunc, wait, immediate));
                return this;
            },
            $importFile: function (api, accept, params, callback, isMultiple) {
                this.$debounce('click', function (e) {
                    var $input = $('<input type=file>');
                    var fileField = 'file';
                    if (accept) {
                        var _accept = [].concat(accept);
                        _accept = _accept.map(i => '.' + i).join(',');
                        $input.attr('accept', _accept);
                    }
                    if (isMultiple) {
                        fileField = 'files';
                        $input.attr('multiple', true);
                    }
                    $input.on('change', function (e) {
                        var loading = new $.jqcLoading()
                        var formData = new FormData();
                        for (var index = 0; index < this.files.length; index++) {
                            var file = this.files[index];
                            formData.append(fileField, file);
                            
                        }
                        var _params = params || {};
                        if (typeof params === 'function') {
                            _params = params(this.files);
                            if (!_params) {
                                return;
                            }
                        }
                        for (var key in _params) {
                            if (_params.hasOwnProperty(key)) {
                                var value = _params[key];
                                formData.append(key, value);
                            }
                        }
                        loading.show('导入中...').lock()
                        $.ajax({
                            type: 'POST',
                            url: api,
                            dataType: "json",
                            data: formData,
                            timeout: 0,
                            processData: false,
                            contentType: false,
                            success: function(res) {
                                loading.unlock().hide()
                                loading = null
                                if (typeof callback === 'function') {
                                    callback(res);
                                    return;
                                }
                                if(res.code == 0){
                                    $.tip('success','导入成功');
                                } else if (res.code == 1) {
                                    $.tip('error', '导入失败', res.msg);
                                }
                            },
                            error: function (res) {
                                loading.unlock().hide()
                                loading = null
                                $.jqcNotification({
                                    type: 'error',
                                    title: '服务器忙，请稍后再试！'
                                });
                            }
                        });
                    });
                    setTimeout(() => {
                        $input[0].click();
                    });
                });
            },
            $importFiles(api, accept, params, callback) {
                this.$importFile(api, accept, params, callback, true);
            },
            // 输入框推荐
            $suggest: function (data, adapter, width) {
                var _data = data;
                var _adapter = adapter;
                if ($.isEnum(data)) {
                    _data = data.values();
                    _adapter = {
                        label: 'desc'
                    };
                }
                this.each(function (index, el) {
                    $(el).$destroy();
                    new $.jqcInput({
                        mode: 'suggest',
                        el: el,
                        data: _data,
                        icon: true,
                        width: width,
                        adapter: _adapter
                    });
                });
                return this;
            },
            $autoComplete: function (config) {
                return this.each(function (index, el) {
                    $(el).$destroy();
                    var _config = Object.assign({
                        mode: 'suggest',
                        el,
                        icon: true
                    }, config);
                    new $.jqcInput(_config);
                });
            },
            // 时间选择
            $timepicker: function (params, defaultValue) {
                var now = new Date();
                return this.each(function (index, el) {
                    var $el = $(el);
                    $el.$destroy();
                    var selfConfig = {};
                    var startYear = $el.attr('startyear');
                    if (startYear != undefined) {
                        selfConfig.startTime = now.setFullYear(calculateYearFromNowByString(startYear));
                    }
                    var endYear = $el.attr('endyear');
                    if (endYear != undefined) {
                        selfConfig.endTime = now.setFullYear(calculateYearFromNowByString(endYear));
                    }
                    var config = Object.assign({}, {
                        el: el,
                        mode: 'timepicker',
                        format: 'yyyy年MM月',
                        getValueFormatter: function (timestamp) {
                            var date =  new Date(timestamp);
                            return date.getFullYear() + String(date.getMonth() + 1).padStart(2, '0');
                        }
                    }, params, selfConfig);
                    new $.jqcInput(config);
                    $el.val(defaultValue);
                });
            },
            // 获取值
            $getValue: function (defaultValue) {
                var _this = this;
                var $el = $(this);
                var type = $.trim($el.attr('datatype')).toLowerCase();
                var value = $el.val();
                if (value === '' || value === undefined) {
                    return defaultValue !== undefined ? defaultValue : '';
                }
                switch (type) {
                    case 'number':
                    case 'int':
                    case 'yuan':
                        value = +value;
                        break;
                    case 'wan':
                        value = $mul(+value, 10000);
                        break;
                    case 'yi':
                        value = $mul(+value, 100000000);
                        break;
                    case 'percent':
                        value = $div(+value, 100);
                        break;
                    case 'date':
                        if ($el.attr('keep') != undefined) {
                            break;
                        }
                        value = $.jqcDateUtil.toMilliSeconds(value);
                        if ($el.attr('end') != undefined) {
                            value += 86399999;
                        }
                        break;
                    default:
                        break;
                }
                return value;
            },
            // checkbox复选框
            $checkbox: function (data, defaultValue, params) {
                return this.each(function (index, el) {
                    var $el = $(el);
                    if ($el.attr('off') !== undefined) {
                        return;
                    }
                   var tipId = 'tipId' + $.jqcToolkit.uuid(8);
                   $el.attr('tip:for', tipId);
                    var dv = $.trim($el.attr('defaultvalue'));
                    defaultValue = dv !== '' ? dv : defaultValue;
                    var config = {
                        el: $el,
                        type: 'checkbox',
                        mode: 'multiple',
                        defaultValue,
                        onChange: function (val) {
                            $el.trigger('change', val);
                        }
                    };
                    var adapter = {
                        value: 'value',
                        label: 'label'
                    };
                    var type = $.isEnum(data) ? 'Enum' : $.jqcToolkit.rawType(data);
                    switch (type) {
                        case 'Enum':
                            config.data = data.values();
                            adapter.label = 'desc';
                            break;
                        case 'Array':
                            config.data = data;
                            break;
                        case 'Object':
                            Object.assign(config, data);
                        default:
                            break;
                    }
                    var c = Object.assign({
                        adapter
                    }, config, params);
                    const instance = new $.jqcCheckbox(c);
                    instance.container.addClass(tipId);
                });
            },
            // radio单选框
            $radio: function (data, defaultValue, params) {
                var config = Object.assign({}, {
                    type: 'radio',
                    mode: 'single'
                }, params);
                return this.$checkbox(data, defaultValue, config);
            },
            // 快捷获取databind元素
            $find: function (dataField) {
                return this.find(`[databind=${dataField}]`);
            },
            // 异步下拉框
            $asyncSelect: function (config, componentConfig = {}) {
                return this.each(function (index, el) {
                    var $this = $(el);
                    var isMultiple = $this.attr('multiple') !== undefined;
                    var _defaultValue = $.trim($this.attr('defaultvalue'));
                    var _config = Object.assign({}, {
                        el: el,
                        mode: 'asyncSelect',
                        searchUrl: 'autoCompletion/suggest',
                        fetchDataUrl: 'autoCompletion/get',
                        isMultiple: config.isMultiple || isMultiple,
                        requestParams: function (question) {
                            return {
                                id: config.id,
                                question: question
                            };
                        },
                        formatResult: function (res) {
                            return res.result || [];
                        },
                        adapter: config.adapter
                    }, componentConfig);
                    if (config.defaultValue) {
                        _config.defaultValue = config.defaultValue;
                    }
                    if (_defaultValue) {
                        _config.defaultValue = _defaultValue;
                    }
                    new $.jqcInput(_config);
                });
            }
            
        });
       /**
         * 从autoCompletion/get 获取值
         * @id 接口id参数
         * @question 接口查询参数 String/Array
         */
        const autoCompletionDataPool = new Map();
        $.getAutoCompletionData = function (id, question) {
            if (!id) {
                throw ('TypeError：请传递参数【id/question】！');
            }
            if(!question || question.length === 0) {
                console.warn('question参数未传递，无法查询到数据!');
                return undefined;
            }
            
            let result = [], // 返回数据
                _this = this,
                queryQuestion = question.split(','); // 将question转换成数组

            // 判断请求参数：部分请求/全未请求/全部请求
            let noQueryQuestion = []; // 未请求过question
            queryQuestion.forEach(query => {
                autoCompletionDataPool.has(query) ? null : noQueryQuestion.push(query);
            })
            // 存在未请求参数
            if (noQueryQuestion.length !== 0) {
                this[`${id}${question}`] ? this[`${id}${question}`].abort() : null;
                this[`${id}${question}`] ? '' : this[`${id}${question}`] = $.ajax({
                    url: 'autoCompletion/get?id=' + id + '&question=' + noQueryQuestion,
                    method: 'GET',
                    async: false,
                    success: function (res) {
                        // res is Array
                        noQueryQuestion.forEach(val => {
                            res.result && res.result.forEach((r, index) => {
                                for (const key in r) {
                                    if (r.hasOwnProperty(key)) {
                                        const element = r[key];
                                        if (element === val) {
                                            autoCompletionDataPool.set(val, r);
                                            break;
                                        }
                                    }
                                }
                            })
                        })
                    },
                    complete: function () {
                        _this[`${id}${question}`] = null;
                    }
                })
            }
            // 统一获取数据，按照请求顺序排序
            queryQuestion.forEach(query => {
                autoCompletionDataPool.get(query) ? result.push(autoCompletionDataPool.get(query)) : '';
            })
            return result;
        }
        /**
         * @description 将数字形式的货币金额转换成中文大写，最大支持16位Number形数字，
         * 大于16位请转换成 字符串传入
         * @param {需要转换的货币数值} dValue 
         */
        $.arabicNumberToChinese = function (dValue, maxDec) {
            // 验证输入金额数值或数值字符串：
            dValue = dValue.toString().replace(/,/g, "");
            dValue = dValue.replace(/^0+/, ""); // 金额数值转字符、移除逗号、移除前导零
            // 分割字符串 去除多余小数点
            let tempValue = dValue.split('.');
            if (tempValue && tempValue.length > 1 && Array.isArray(tempValue)) {
                let temp = '';
                tempValue.forEach((val, index) => {
                    index === 0 ? temp += `${val}.` : temp += val;
                })
                dValue = temp;
                temp = null;
            }
            if (dValue == "") {
                return "零";
            } // （错误：金额为空！）
            else if (isNaN(dValue)) {
                return "错误：金额不是合法的数值！";
            }
            var minus = ""; // 负数的符号“-”的大写：“负”字。可自定义字符，如“（负）”。
            var CN_SYMBOL = ""; // 币种名称（如“人民币”，默认空）
            if (dValue.length > 1) {
                if (dValue.indexOf('-') == 0) {
                    dValue = dValue.replace("-", "");
                    minus = "负";
                } // 处理负数符号“-”
                if (dValue.indexOf('+') == 0) {
                    dValue = dValue.replace("+", "");
                } // 处理前导正数符号“+”（无实际意义）
            }
            // 变量定义：
            var vInt = "";
            var vDec = ""; // 字符串：金额的整数部分、小数部分
            var resAIW; // 字符串：要输出的结果
            var parts; // 数组（整数部分.小数部分），length=1时则仅为整数。
            var digits, radices, bigRadices, decimals; // 数组：数字（0~9——零~玖）；基（十进制记数系统中每个数字位的基是10——拾,佰,仟）；大基（万,亿,兆,京,垓,杼,穰,沟,涧,正）；辅币（元以下，角/分/厘/毫/丝）。
            var zeroCount; // 零计数
            var i, p, d; // 循环因子；前一位数字；当前位数字。
            var quotient, modulus; // 整数部分计算用：商数、模数。
            // 金额数值转换为字符，分割整数部分和小数部分：整数、小数分开来搞（小数部分有可能四舍五入后对整数部分有进位）。
            var NoneDecLen = (typeof (maxDec) == "undefined" || maxDec == null || Number(maxDec) < 0 || Number(maxDec) > 5); // 是否未指定有效小数位（true/false）
            parts = dValue.split('.'); // 数组赋值：（整数部分.小数部分），Array的length=1则仅为整数。
            if (parts.length > 1) {
                vInt = parts[0];
                vDec = parts[1]; // 变量赋值：金额的整数部分、小数部分
                // 小数只保留两位
                vDec = vDec.substr(0, 2)
                if (NoneDecLen) {
                    maxDec = vDec.length > 5 ? 5 : vDec.length;
                } // 未指定有效小数位参数值时，自动取实际小数位长但不超5。
                var rDec = Number("0." + vDec);
                rDec *= Math.pow(10, maxDec);
                rDec = Math.round(Math.abs(rDec));
                rDec /= Math.pow(10, maxDec); // 小数四舍五入
                var aIntDec = rDec.toString().split('.');
                if (Number(aIntDec[0]) == 1) {
                    vInt = (Number(vInt) + 1).toString();
                } // 小数部分四舍五入后有可能向整数部分的个位进位（值1）
                if (aIntDec.length > 1) {
                    vDec = aIntDec[1];
                } else {
                    vDec = "";
                }
            } else {
                vInt = dValue;
                vDec = "";
                if (NoneDecLen) {
                    maxDec = 0;
                }
            }
            if (vInt.length > 44) {
                return "错误：数值过大！整数位长【" + vInt.length.toString() + "】超过了上限！";
            }
            // 准备各字符数组 Prepare the characters corresponding to the digits:
            digits = new Array("零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"); // 零~玖
            radices = new Array("", "拾", "佰", "仟"); // 拾,佰,仟
            bigRadices = new Array("", "万", "亿", "兆", "京", "垓", "杼", "穰", "沟", "涧", "正"); // 万,亿,兆,京,垓,杼,穰,沟,涧,正
            decimals = new Array("角", "分", "厘", "毫", "丝"); // 角/分/厘/毫/丝
            resAIW = ""; // 开始处理
            // 处理整数部分（如果有）
            if (Number(vInt) > 0) {
                zeroCount = 0;
                for (i = 0; i < vInt.length; i++) {
                    p = vInt.length - i - 1;
                    d = vInt.substr(i, 1);
                    quotient = p / 4;
                    modulus = p % 4;
                    if (d == "0") {
                        zeroCount++;
                    } else {
                        if (zeroCount > 0) {
                            resAIW += digits[0];
                        }
                        zeroCount = 0;
                        resAIW += digits[Number(d)] + radices[modulus];
                    }
                    if (modulus == 0 && zeroCount < 4) {
                        resAIW += bigRadices[quotient];
                    }
                }
                resAIW;
            }
            // 处理小数部分（如果有）
            for (i = 0; i < vDec.length; i++) {
                d = vDec.substr(i, 1);
                i == 0 ? resAIW += '元' : '';
                if (d != "0") {
                    resAIW += digits[Number(d)] + decimals[i];
                }
            }
            // 处理结果
            if (resAIW == "") {
                resAIW = "零";
            } // 零元
            if (vDec == "") {
                resAIW += '元整';
            } // ...元整
            resAIW = CN_SYMBOL + minus + resAIW; // 人民币/负......元角分/整
            return resAIW;
        }
        /**
         * @description 获取URL地址中的参数
         * @param {*} name url
         */
        $.getQueryString = function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return unescape(r[2]);
            }
            return null;
        }
        $.prototype.$select = $.prototype.select;
        $.prototype.$selectSearch = $.prototype.selectSearch;
        $.prototype.$destroy = $.prototype.destroySelectBox;
        /* ********************************************************** */
        // ajax全局提醒
        $.ajaxSetup({
            timeout: 45000,     // 45秒
            complete: function (xhr, status) {
                // 登陆已过期
                if (xhr.responseJSON && xhr.responseJSON.code && xhr.responseJSON.code == 401) {
                    Loading.unlock().hide();
                    $.jqcConfirm({
                        title: '未登录',
                        content: '您的登录状态已过期,请刷新页面！',
                        cancelText: false,
                        onConfirm: function() {
                            window.location.reload();
                        }
                    });
                }
            },
            // 请求失败
            error: function (xhr, status, error) {
                Loading.unlock().hide();
                if (error === 'abort') {
                    return;
                }
                var msg = '服务器连接失败';
                if (error === 'timeout') {
                    msg = '请求超时';
                }
                $.jqcNotification({
                    type: 'error',
                    title: msg
                });
            }
        });
        /**
         * 缓存api请求到的数据
         */
        $.DataPool = function (apisMap) {
            this.map = new Map();
            this.apisMap = apisMap || {};
            this.clear();
        }
        $.DataPool.prototype = {
            set: function (key, value) {
                this.map.set(key, value);
            },
            get: function (key) {
                if (typeof key === 'string') {
                    return this.map.get(key);
                }
                if ($.jqcToolkit.rawType(key) === 'Array') {
                    return key.map(i => this.get(i));
                }
                throw new Error('function get params expect String/Array<String>');
            },
            asyncGet: function (key, reload) {
                var _this = this;
                if (typeof key === 'string') {
                    return new Promise(function (resolve, reject) {
                        if (!_this.apisMap[key]) {
                            reject(`storage: 获取${key}的api地址不存在！`);
                            return;
                        }
                        var result;
                        var data = _this.map.has(key);
                        if (data && !reload) {
                            resolve(_this.map.get(key));
                        } else {
                            _this.update(key).then(function (data) {
                                resolve(data);
                            });
                        }
                    });
                }
                if ($.jqcToolkit.rawType(key) == 'Array') {
                    return Promise.all(key.map(i => this.asyncGet(i, reload)));
                }
                throw new Error('function asyncGet params expect String/Array<String>');
            },
            update: function (key) {
                var _this = this;
                if (typeof key === 'string') {
                    return new Promise((resolve, reject) => {
                        var value = _this.apisMap[key];
                        var api;
                        var cb;
                        if ($.jqcToolkit.rawType(value) == 'Undefined') {
                            reject(`DATA_POOL: ${key}的api地址不存在！`);
                            return;
                        }
                        if ($.jqcToolkit.rawType(value) == 'String') {
                            api = value;
                        } else if ($.jqcToolkit.rawType(value) == 'Object') {
                            api = value.api;
                            cb = value.preprocess;
                        }
                        $.ajax(api).then(res => {
                            if (res.code == 0) {
                                var data = res.result || [];
                                if ($.jqcToolkit.rawType(cb) == 'Function') {
                                    data = cb(data);
                                }
                                _this.map.set(key, data);
                                resolve(data);
                            } else if (res.code == 1) {
                                $.tip('error', '请求接口失败', res.msg);
                                console.error('请求接口失败：' + api);
                                reject(res.msg);
                            }
                        });
                    });
                }
                if ($.jqcToolkit.rawType(key) === 'Array') {
                    return Promise.all(key.map(i => this.update(i)));
                }
                throw new Error('function update params expect String/Array<String>');
            },
            clear: function () {
                this.map.clear();
            },
            delete: function (key) {
                if (typeof key === 'string') {
                    this.map.delete(key);
                } else if ($.jqcToolkit.rawType(key) === 'Array') {
                    key.forEach(i => {
                        this.delete(i);
                    })
                }
            }
        };
        // 兼容老代码
        $.SessionStorage = $.DataPool;
        /* ********************************************************** */

        // dxDataGrid columns快速格式化
        /**
         * 
         * @param {String} caption      // 列名
         * @param {String} dataField    // 对应字段名称
         * @param {Number} width        // 列宽
         * @param {Object | Enum} extParams   // 可拓展参数
         * @param {Object} extConfig 排序类拓展参数
         */
        $.dxColumn = function (caption, dataField, width, extParams, extConfig) {
            var baseConfig = {
                caption: caption,
                dataField: dataField || undefined,
                width: width || undefined,
                cssClass: 'min_width_' + caption.length * 20
            };
            var config = {};
            // 默认
            if (extParams === undefined) {
                return Object.assign({}, baseConfig);
            }
            var extType = extParams.constructor.name;
            if ($.isEnum(extParams)) {
                extType = 'Enum';
            }
            var dataType;
            var arg1;
            if (extType === 'Object') {    // 拓展参数
                config = extParams;
            } else if (extType === 'Enum') {   // 枚举
                config = {
                    lookup: {
                        dataSource: extParams.values(),
                        displayExpr: 'desc',
                        valueExpr: 'value'
                    }
                };
            } else if (extType === 'Function') {
                config.calculateCellValue = function (rowData) {
                    return extParams(rowData[dataField], rowData);
                }
            } else if (extType === 'String') {     // 字符串
                var arg = extParams.split(',');
                dataType = arg[0];
                arg1 = arg[1];
            }
            switch (dataType) {
                case 'date':
                    config = {
                        dataType: 'date',
                        format: extConfig && extConfig.timeFormat ? $.timeFormat(extConfig.timeFormat) : $.timeFormat(),
                        width: width || 150,
                        calculateCellValue: function (rowData) {
                            var value = rowData[dataField];
                            if ($.trim(value) === '') {
                                return undefined;
                            }
                            if (!isNaN(value)) {
                                return new Date(+value);
                            }
                            if (typeof value === 'string') {
                                return new Date(value);
                            }
                        }
                    };
                    break;
                case 'datetime':
                    config = {
                        dataType: 'date',
                        format: extConfig && extConfig.timeFormat ? $.timeFormat(extConfig.timeFormat) : $.timeFormat('yyyy-MM-dd HH:mm:ss'),
                        width: width || 180,
                        calculateCellValue: function (rowData) {
                            var value = rowData[dataField];
                            if ($.trim(value) === '') {
                                return undefined;
                            }
                            if (!isNaN(value)) {
                                return new Date(+value);
                            }
                            if (typeof value === 'string') {
                                return new Date(value);
                            }
                        }
                    };
                    break;
                case 'boolean':
                    config = {
                        lookup: {
                            dataSource: [{
                                value: '1',
                                label: '是'
                            }, {
                                value: '0',
                                label: '否'
                            }],
                            displayExpr: 'label',
                            valueExpr: 'value'
                        }
                    };
                    break;                   
                case 'currency':
                    config = {
                        dataType: 'number',
                        format: {
                            type: 'fixedPoint',
                            precision: arg1
                        }
                    }
                    break;
                case 'number':
                    config = {
                        dataType: 'number',
                        format: function (value) {
                            return $.numberFormatter(value, arg1);
                        }
                    }
                    break;
                case 'percent':
                    config = {
                        dataType: 'number',
                        format: {
                            type: 'percent',
                            precision: arg1
                        }
                    }
                    break;
                case 'yuan':
                    config = {
                        dataType: 'number',
                        format: function (value) {
                            return $.currencyFormatter(value, arg1, '', '元')
                        }
                    }
                    break;
                case 'wan':
                    config = {
                        dataType: 'number',
                        format: function (value) {
                            if (isNaN(value)) {
                                return '';
                            }
                            var _value = value / 10000;
                            return $.currencyFormatter(_value, arg1, '', '万')
                        }
                    }
                    break;
                case 'yi':
                    config = {
                        dataType: 'number',
                        format: function (value) {
                            if (isNaN(value)) {
                                return '';
                            }
                            var _value = value / 100000000;
                            return $.currencyFormatter(_value, arg1, '', '亿')
                        }
                    }
                    break;
                default:
                    break;
            }
            return Object.assign({}, baseConfig, Object.assign({}, config, extConfig));
        };
        /**
         * params.url 跳转地址
         * params.formId 页面地址
         * params.title 页面title
         * params.data  searchBar填充数据
         */
        $.open = function (params, openInThisTab) {
            var url = params.url;
            var queryObject = {}
            if (params.formId) {
              queryObject.formId = params.formId;
            }
            if (params.title) {
              queryObject.title = params.title;
            }
            if ($.type(params.data) === 'object') {
              Object.assign(queryObject, params.data);
            }
            if (!$.isEmptyObject(queryObject)) {
                url += '?' + $.param(queryObject);
            }
            if (openInThisTab) {
                window.location.href = url;
            } else {
                window.open(url);
            }
        }
        var jqcTab = null;
        // 跳转进入系统
        var queryStringObject = $.jqcToolkit.getQueryStringObject();
        $.addForm = function(menu, tab, needReload, parentId) {
            var uid = menu.url;
            var text = menu.text;
            var data = menu.data;
            if (queryStringObject.formId) {
                uid = queryStringObject.formId;
                text = queryStringObject.title;
                data = Object.assign({}, queryStringObject, {
                    formId: undefined,
                    title: undefined
                });
                queryStringObject = {};
            }
            if (uid.indexOf('http') == 0) {
                window.open(uid);
                return;
            }
            if (!jqcTab) {
                jqcTab = tab;
            }
            // tab内存在
            if (tab.index.has(uid)) {
                tab.add({
                    id: uid,
                    title: text
                });
                if (needReload) {
                    var panel = tab.index.get(uid).panel;
                    var root = panel.find('div[data-tabid]');
                    root.empty();
                    var _app = $.getFormCache(uid);
                    _app.mount(root, data);
                }
                return;
            }
            var _path = APP_ROOT_PATH.concat(uid);
            // 第一次点击menu
            if (!$.formCacheHas(uid)) {
                $._globalCacheId = uid;
                $JqcLoader
                    .importScript(_path)
                    .execute(function() {
                        addTabAndCreatePage(_path, text, parentId);
                    });
            } else {
                // 关闭后再次点击menu
                addTabAndCreatePage(_path, text, parentId);
            }
            function addTabAndCreatePage(path, text, parentId) {
                tab.add({
                    id: uid,
                    title: text,
                    content: `<div data-tabid=${uid} data-path=${path} data-name=${text} style="height:100%"></div>`,
                    beforeDestroy: function () {
                        var inputs = this.panel.find('input');
                        inputs.destroySelectBox();
                        var _app = formCache.get(uid);
                        if (_app) {
                            var _beforeDestroy = [function(next) {
                                $(document).trigger('click');
                                next();
                            }].concat(_app._beforeDestroy);
                            if ($.App.beforeDestroy) {
                                _beforeDestroy.unshift($.App.beforeDestroy);
                            }
                            queue(_beforeDestroy, _app);
                        }
                        if (parentId) {
                            var parentTab = jqcTab.index.get(parentId);
                            if (parentTab) {
                                setTimeout(function () {
                                    parentTab.tab.trigger('click');
                                }, 16)
                            }
                        }
                    },
                    onActive: function () {
                        $('.dx-datagrid-column-chooser .dx-closebutton').each(function (index, el) {
                            el.click();
                        });
                        var _app = formCache.get(uid);
                        if (_app && typeof _app.__onShow__ === 'function') {
                            _app.__onShow__();
                        }
                    },
                    onShow: function () {
                        $('.dx-datagrid-column-chooser .dx-closebutton').each(function (index, el) {
                            el.click();
                        });
                    },
                    afterRender: function (panel) {
                        var root = panel.find('div[data-tabid]');
                        setTimeout(function() {
                            $.getFormCache(uid).mount(root, data);
                        }, 10);
                    }
                });
            }
        };
        $.openPage = function (formUrl, title, data, isReload) {
            var needReload = (isReload === undefined || isReload) ? true : false;
            $.addForm({
                text: title,
                url: formUrl,
                data: data
            }, jqcTab, needReload);
        }
        $.closePage = function (formUrl) {
            var $closeBtn = $('[closeid="'+formUrl+'"]');
            if ($closeBtn.length) {
                $closeBtn.trigger('click');
            } else {
                console.warn('$.closePage:未找到需要关闭的页面。');
            }
        }
        // 缓存新加载的js文件数据
        const formCache = new Map();
        // set cache
        $.setupApp = function(App) {
            if (App.mount) {
                if (formCache.has($._globalCacheId)) {
                    return;
                }
                formCache.set($._globalCacheId, App);
            } else {
                throw new Error('$.setupApp: expects a $.App object');
            }
            $.jqcEvent.emit('setup.app', App);
        };
        // get cache
        $.getFormCache = function(uid) {
            return formCache.get(uid);
        };
        // has cache ?
        $.formCacheHas = function(uid) {
            return formCache.has(uid);
        };
        // download file
        $.downloadFile = function (url, type) {
            $.ajax({
                url: url,
                xhrFields: {
                    responseType: 'blob'
                }
            }).then((res, text, request) => {
                if (!res.code) {
                    var fileName = decodeURI(escape(request.getResponseHeader('content-disposition').split('filename="')[1].split('"')[0]))
                    var url = window.URL.createObjectURL(new Blob([res], {
                        type: 'application/octet-stream'
                    }));
                    var link = document.createElement('a');
                    link.style = 'display:none';
                    link.href = url;
                    link.target = '_blank';
                    link.download = fileName;
                    link.click();
                    window.URL.revokeObjectURL(url);
                } else {
                    pb.failNotice('下载失败！');
                }

            }).catch(error => {
                pb.failNotice('下载失败！');
            })
        }
        // App页面核心
        $.App = function (params) {
            var _this = this;
            this._root = null; //容器页面根节点
            this.root = null; //暴露给afterRender的容器根节点
            this._path = ''; //js文件路径
            this.mixinFormat = [];
            this._beforeRender = [];
            this._afterRender = [];
            this.components = [];
            this._dataSource = [];
            this._beforeDestroy = [];
            this.__script__ = params.scriptPath || null;    // 依赖的script
            this.__options__ = params || {};
            this.loading = new $.jqcLoading();
            this.pinyinParser = pinyinParser;
            this.$eventBus = $('<div>');    // 用于绑定、监听、触发事件
            if (params.dxDataGrid && params.dxDataGrid.hideIndex) {
                this._config = $.getGlobalConfig(true);
            } else {
                this._config = $.getGlobalConfig();
            }
            Object.defineProperty(this, 'dataSource', {
                get: function () {
                    return _this._dataSource;
                },
                set: function (val) {
                    _this._dataSource = [].concat(val);
                    $.jqcEvent.emit('dataSourceLoaded.app', val, _this);
                }
            });
            $.setupApp(this);
        };
        /**
         * name: 系统名称（必须）
         * id: 系统id（必须）
         * beforeRender（next）: 每个页面共有的beforeRender
         * afterRender(next, options): 每个页面共有的afterRender
         * beforeDestroy(next)： 每个页面共有的beforeDestroy
         * beforeInit(next): 系统初始化之前回调
         * logoutRedirect: 注销时重定向地址
         * menu: menu配置项
         * reportBrowserInfo: 是否上报浏览器信息，默认false
         * notify: {
         *     
         * }
         */
        $.App.init = function (params) {
            let sysId = '';
            let sysName = params.name;
            const pageType = window.__PAGE_TYPE__;
            if (params && typeof params.id === 'string') {
                sysId = params.id.toUpperCase();
            } else {
                $.tip('error', '系统初始化失败', '请设置系统ID', 0);
                return;
            }
            $.App.beforeRender = typeof params.beforeRender === 'function' ? params.beforeRender : null;
            $.App.afterRender = typeof params.afterRender === 'function' ? params.afterRender : null;
            $.App.beforeDestroy = typeof params.beforeDestroy === 'function' ? params.beforeDestroy : null;
            var tab;
            var $tab = $('.app-tab');
            var menu;
            const initQueue = [
                function beforeInit(next) {
                    Loading.show('加载中...');
                    if (sysName) {
                        $('.app-menu-logo').text(sysName);
                        document.title = sysName;
                    }
                    if ($.jqcToolkit.rawType(params.beforeInit) === 'Function') {
                        params.beforeInit(next);
                    } else {
                        next();
                    }
                },
                function init(next) {
                    $('.app-logout').on('click', function(e) {
                        $.ajax({
                            url: 'open/logout',
                            success: function(data) {
                                window.location = params.logoutRedirect || 'http://www.orientsec.com.cn';
                            }
                        });
                    });
                    if (pageType !== 'todo' && pageType !== 'oaTodo') {
                        tab = new $.jqcTab({
                            element: $('.app-tab'),
                            position: 'absolute'
                        });
                    } else {
                        Loading.lock();
                        $tab.remove();
                    }
                    const requests = [];
                    requests.push($.ajax('user/getCurrentUser'));
                    if (pageType !== 'todo' && pageType !== 'oaTodo' && params.menu !== false) {
                        requests.push($.ajax({
                            url: 'menu/queryUserMenu',
                            data: { sysId }
                        }));
                    }
                    Promise.all(requests).then(res => {
                        $.__userInfo__ = res[0].result;
                        $('.app-user').text(res[0].result.displayName);
                        // 菜单初始化
                        if (res[1]) {
                            const menuConfig = Object.assign({
                                data: res[1].result.pinned || res[1].result.authorized || [],
                                top: 40,
                                left: 0,
                                autoSkip: true,
                                autoHide: false,
                                allowedConfig: true,
                                triggerFirst: true,
                                configBoxWidth: 500,
                                speed: 100,
                                configurableMenuData: res[1].result.authorized || [],
                                adapter: {
                                    id: 'id',
                                    label: 'name',
                                    child: 'subMenuList'
                                },
                                onSelect: function(menu) {
                                    var _menu = {
                                        id: menu.id,
                                        text: menu.name,
                                        url: menu.form
                                    };
                                    $.addForm(_menu, tab);
                                },
                                onResettingMenu: function(menu) {
                                    $.ajax({
                                        url: 'menu/updateUserCustMenu',
                                        method: 'POST',
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        data: JSON.stringify({
                                            sysId,
                                            menuIdList: menu
                                        })
                                    });
                                },
                                onShow: function (width, speed) {
                                    $tab.animate({
                                        'padding-left': width + 10
                                    }, speed, function () {
                                        $.jqcEvent.emit('resize.menu');
                                    });
                                },
                                onHide: function (width, speed) {
                                    $tab.animate({
                                        'padding-left': 30
                                    }, speed, function () {
                                        $.jqcEvent.emit('resize.menu');
                                    });
                                }
                            }, params.menu);
                            menu = new $.jqcMenuTree(menuConfig);
                        }
                        next();
                    });
                },
                function afterInit(next) {
                    if (params.reportBrowserInfo && !localStorage.getItem('browser_info_is_reported')) {
                        setTimeout(function () {
                            var Browser = {
                                userAgent: navigator.userAgent,
                                info: function () {
                                    var userAgent =  this.userAgent.toLowerCase();
                                    var coreType = this.coreType();
                                    var info = {
                                        type: 11,
                                        core: coreType,
                                        version: undefined
                                    };
                                    if (userAgent.indexOf('qqbrowser') > -1) {
                                        info.type = 6;
                                    } else if (userAgent.indexOf('360se') > -1) {
                                        info.type = 5;
                                    } else if (userAgent.indexOf('metasr') > -1) {
                                        info.type = 9;
                                    } else if (userAgent.indexOf('lbbrowser') > -1) {
                                        info.type = 10;
                                    } else if (userAgent.indexOf('opera') > -1 || userAgent.indexOf('opr') > -1) {
                                        info.type = 8;
                                        info.version = userAgent.match(/version\/\S+/g)[0].slice(8);
                                    } else if (userAgent.indexOf('edge') > -1) {
                                        info.type = 4;
                                        info.version = userAgent.match(/edge\/\S+/g)[0].slice(5);
                                    } else if (userAgent.indexOf('firefox') > -1) {
                                        info.type = 2;
                                        info.version = userAgent.match(/firefox\/\S+/g)[0].slice(8);
                                    } else if (userAgent.indexOf('chrome') > -1) {
                                        info.type = 1;
                                    } else if (userAgent.indexOf('safari') > -1) {
                                        info.type = 7;
                                        info.version = userAgent.match(/version\/\S+/g)[0].slice(8);
                                    } else if (userAgent.indexOf('msie') > -1 || userAgent.indexOf('trident') > -1) {
                                        info.type = 3;
                                    }
                                    // 多核心
                                    if (userAgent.indexOf('chrome') > -1) {
                                        info.version = userAgent.match(/chrome\/\S+/g)[0].slice(7);
                                    } else if (userAgent.indexOf('msie') > -1) {
                                        info.version = userAgent.match(/msie\s[\d\.]+/g)[0].slice(5);
                                    } else if (userAgent.indexOf('trident') > -1) {
                                        info.version = userAgent.match(/rv\:[\d\.]+/g)[0].slice(3);
                                    }
                                    return info;
                                },
                                coreType: function () {
                                    var userAgent =  this.userAgent.toLowerCase();
                                    if (userAgent.indexOf('webkit') > -1) {
                                        return 4;
                                    }
                                    if (userAgent.indexOf('trident') > -1) {
                                        return 2;
                                    }
                                    if (userAgent.indexOf('blink') > -1) {
                                        return 1;
                                    }
                                    if (userAgent.indexOf('presto') > -1) {
                                        return 5;
                                    }
                                    if (userAgent.indexOf('gecko') > -1) {
                                        return 3;
                                    }
                                    return 6;
                                }
                            };
                            var info = Browser.info();
                            var browserInfo = {
                                width: window.innerWidth,
                                height: window.innerHeight,
                                browserType: info.type,
                                browserCoreType: info.core,
                                version: info.version,
                                agentInfo: window.navigator.userAgent
                            }
                            $.post('clientInfo/add', browserInfo, function (res) {
                                // 上报成功标识
                                if (res.code == 0) {
                                    localStorage.setItem('browser_info_is_reported', true);
                                }
                            });
                        }, 20000);
                    }
                    if (pageType === 'todo') {
                        openTodoPage();
                        return;
                    }
                    if (pageType === 'oaTodo') {
                        openOaTodoPage();
                        return;
                    }
                    if (params.notify) {
                        var notifyConfig = $.type(params.notify) === 'object' ? params.notify : {};
                        var $notify = $('<div>').addClass('app-notify');
                        $('.app-user').before($notify);
                        $.App.notify = new $.jqcNotify({
                            el: $notify,
                            position: 'right',
                            adapter: notifyConfig.adapter || {
                                title: 'msg',
                                time: 'updateTime'
                            },
                            tip: function (data) {
                                return data.length ? '' : '暂无通知';
                            },
                            onSelect: function (data, next) {
                                if ($.type(notifyConfig.onSelect) === 'function') {
                                    notifyConfig.onSelect(data, function () {
                                        $.post('appSystemTodo/markdoneByTodoType', data).then(res => {
                                            updateNotifyDataSource();
                                        });
                                    });
                                } else {
                                    next();
                                }
                            }
                        });

                        function updateNotifyDataSource() {
                            $.ajax({
                                method: 'GET',
                                url: notifyConfig.url ||　'appSystemTodo/query',
                                data: notifyConfig.params ? notifyConfig.params : {}
                            }).then(res => {
                                if (res.code != '0') {
                                    return;
                                }
                                $.App.notify.update(res.result || []);
                            });
                        }
                        updateNotifyDataSource();
                    }
                    next();
                },
                function done() {
                    if (typeof params.afterInit === 'function') {
                        params.afterInit(tab);
                    }
                }
            ];
            // 执行init队列
            queue(initQueue);

            function openTodoPage() {
                var actionId = getQueryString('actionId');
                var recordId = getQueryString('recordId');
                if (!$.App.$actions) {
                    throw new Error('$.App未启用actions插件');
                }
                var actionConfig = $.App.$actions[actionId];
                // uri
                if ($.type(actionConfig) === 'string') {
                    $._globalCacheId = actionConfig;
                    $('.app_container').attr('data-path', APP_ROOT_PATH + actionConfig).attr('data-uuid', actionConfig);
                    $JqcLoader.importScript(APP_ROOT_PATH + actionConfig).execute(function () {
                        $.jqcEvent.on('setup.app', function (app) {
                            app.mount($('.app_container'))
                            app.$eventBus.on('done.render', function () {
                                Loading.unlock().hide();
                            });
                        });
                    })
                    return;
                }

                // obj
                if ($.type(actionConfig) === 'object') {
                    $.ajax({
                        url: 'todo/auth.wf',
                        data: { recordId: recordId }
                    }).then(res => {
                        if (res.code != 0) {
                            return;
                        }
                        if (res.result.roleId === undefined) {
                            $.tip('error', '打开流程失败', '你无权访问该流程或该流程不存在', 0);
                            return;
                        }
                        var actionForRole = actionConfig[res.result.roleId];
                        if ($.type(actionForRole) !== 'object') {
                            throw new Error(`$.App actions插件: ${actionId}.${res.result.roleId}配置项错误`);
                        }
                        var formId = actionForRole.formId;
                        if ($.type(formId) !== 'string') {
                            throw new Error(`$.App actions插件: ${actionId}.${res.result.roleId}.formId 配置项错误`);
                        }
                        $._globalCacheId = actionId;
                        $('.app_container').attr('data-path', APP_ROOT_PATH + formId).attr('data-uuid', actionId);
                        // 加载js入口文件
                        $JqcLoader.importScript(APP_ROOT_PATH + formId).execute(function () {
                            $.jqcEvent.on('setup.app', function (app) {
                                app.mount($('.app_container'));
                                var action = actionForRole.action;
                                if ($.type(action) !== 'function') {
                                    throw new Error(`$.App actions插件: ${actionId}.${res.result.roleId}.action 必须为function类型`);
                                }
                                app.$eventBus.on('done.render', function (e) {
                                    // 不需要record详情
                                    if (typeof actionConfig.fetchDataApi === 'undefined') {
                                        action.call(app, () => {
                                            Loading.unlock().hide();
                                        }, undefined, res.result, recordId);
                                        return;
                                    }
                                    // 需要record详情
                                    var _queue = [
                                        function beforeFetchData(next) {
                                            if (typeof actionConfig.beforeFetchData === 'function') {
                                                actionConfig.beforeFetchData(next, recordId, app);
                                            } else {
                                                next({
                                                    id: recordId
                                                });
                                            }
                                        },
                                        function fetchData(next, data) {
                                            app.requestGet(actionConfig.fetchDataApi, data).then(res => {
                                                next(res)
                                            });
                                        },
                                        function afterFetchData(next, res) {
                                            if (typeof actionConfig.afterFetchData === 'function') {
                                                actionConfig.afterFetchData(next, res, app);
                                            } else {
                                                next(res.result);
                                            }
                                        },
                                        function todo(next, detail) {
                                            action.call(app, () => {
                                                Loading.unlock().hide();
                                            }, detail, res.result, recordId);
                                        }
                                    ];
                                    queue(_queue);
                                });
                            });
                        });

                    });
                    return;
                }

                // err
                throw new Error(`$.App actions插件: ${actionId}不存在`);
            }
            function openOaTodoPage() {
                /**
                 * actionId：匹配action
                 * recordId：请求权限，获取子项的键
                 */
                var actionId = getQueryString('actionId');
                var recordId = getQueryString('recordId');
                if (!$.App.$actions) {
                    throw new Error('$.App未启用actions插件');
                }
                // 前端配置的action
                var actionConfig = $.App.$actions[actionId];
                // uri
                if ($.type(actionConfig) === 'string') {
                    $._globalCacheId = actionConfig;
                    $('.app_container').attr('data-path', APP_ROOT_PATH + actionConfig).attr('data-uuid', actionConfig);
                    $JqcLoader.importScript(APP_ROOT_PATH + actionConfig).execute(function () {
                        $.jqcEvent.on('setup.app', function (app) {
                            app.mount($('.app_container'))
                            app.$eventBus.on('done.render', function () {
                                Loading.unlock().hide();
                            });
                        });
                    })
                    return;
                }

                // obj
                if ($.type(actionConfig) === 'object') {
                    // 如果只有actionId直接执行里面的action
                    var action = actionConfig.action;
                    if ($.type(action) !== 'function') {
                        throw new Error(`$.App actions插件: ${actionId}.action 必须为function类型`);
                    }

                    if ($.type(actionConfig.formId) !== 'string') {
                        throw new Error(`$.App actions插件: ${actionId}.formId 配置项错误`);
                    }
                    $('.app_container').attr('data-path', APP_ROOT_PATH + actionConfig.formId).attr('data-uuid', actionId);
                    // 加载js入口文件
                    $JqcLoader.importScript(APP_ROOT_PATH + actionConfig.formId).execute(function () {
                        $.jqcEvent.on('setup.app', function (app) {
                            app.mount($('.app_container'));
                            app.$eventBus.on('done.render', function (e) {
                                // 不需要record详情
                                if (typeof actionConfig.fetchDataApi === 'undefined') {
                                    action.call(app, () => {
                                        Loading.unlock().hide();
                                    }, undefined);
                                    return;
                                }
                                // 需要record详情
                                var _queue = [
                                    function beforeFetchData(next) {
                                        if (typeof actionConfig.beforeFetchData === 'function') {
                                            actionConfig.beforeFetchData(next, recordId, app);
                                        } else {
                                            next({
                                                id: recordId
                                            });
                                        }
                                    },
                                    function fetchData(next, data) {
                                        app.requestGet(actionConfig.fetchDataApi, data).then(res => {
                                            next(res)
                                        });
                                    },
                                    function afterFetchData(next, res) {
                                        if (typeof actionConfig.afterFetchData === 'function') {
                                            actionConfig.afterFetchData(next, res, app);
                                        } else {
                                            next(res.result);
                                        }
                                    },
                                    // FetchData的结果
                                    function todo(next, detail) {
                                        action.call(app, () => {
                                            Loading.unlock().hide();
                                        }, detail, recordId);
                                    }
                                ];
                                queue(_queue);
                            });
                        });
                    });
                    return;
                }

                // err
                throw new Error(`$.App actions插件: ${actionId}不存在`);
            }
        }
        // 加载页面
        $.App.prototype.mount = function (root, data) {
            var _this = this;
            this._root = root;
            this.root = root;
            this._path = root.attr('data-path');
            this.$name = root.attr('data-name');
            this._id = root.attr('data-tabid');
            this.loading.show('加载中...');
            // 第一次加载需要加载依赖js文件
            if (!this.__scriptLoaded__) {
                var scripts = [].concat(this.__script__);
                scripts.forEach(script => {
                    if (script) {
                        var _path = _this.getAbsolutePath(script);
                        $JqcLoader.importScript(_path);
                    }
                });
                $JqcLoader.execute(() => {
                    var params = this.__options__;
                    if (params && params.mixins && params.mixins.length) {
                        var keysFlag = {};
                        params.mixins.forEach(mixin => {
                            if (typeof mixin === 'string') {
                                mixin = MIXIN.get(mixin);
                            }
                            for (var key in mixin) {
                                if (key == 'format' && typeof mixin[key] == 'function') {
                                    _this.mixinFormat.push(mixin[key].bind(_this));
                                } else if (key == 'afterRender' && typeof mixin[key] == 'function') {
                                    _this._afterRender.push(mixin[key].bind(_this));
                                } else if (key == 'beforeRender' && typeof mixin[key] == 'function') {
                                    _this._beforeRender.push(mixin[key].bind(_this));
                                } else if (key == 'beforeDestroy' && typeof mixin[key] == 'function') {
                                    _this._beforeDestroy.push(mixin[key].bind(_this));
                                } else if (key == 'components' && T.rawType(mixin[key]) == 'Array') {
                                    _this.components = _this.components.concat(mixin[key]);
                                } else if (key == 'mixins') {
                                    // 忽略
                                } else {
                                    if (keysFlag[key] === undefined) {
                                        keysFlag[key] = _this[key] === undefined ? 0 : 1;       // 0: 可覆盖；1：实例本身属性无法覆盖
                                    }
                                    if (keysFlag[key] === 0) {
                                        _this[key] = typeof mixin[key] === 'function' ? mixin[key].bind(this) : mixin[key];
                                    }
                                }
                            }
                        });
                    }
                    this.components = (params && params.components) ? this.components.concat(params.components) : this.components;  //组件
                    this.templatePath = (params && params.templatePath) ? params.templatePath : (this.templatePath || null); //模板文件相对路径
                    this.stylePath = (params && params.stylePath) ? params.stylePath : (this.stylePath || null); //模板文件相对路径
                    this.contextmenu = (params && params.contextmenu) ? params.contextmenu : (this.contextmenu || null);
                    this.dxDataGrid = (params && params.dxDataGrid) ? params.dxDataGrid : (this.dxDataGrid || null);
                    this.__onShow__ = (typeof params.onShow === 'function') ? params.onShow.bind(this) : null;
                    // 框架集成预处理
                    this.mixinFormat.push(function (root, type, data) {
                        // 防止input自动填充
                        root.find('input').attr('autocomplete', 'off');
                        // textarea 组件化
                        var $textarea = root.find('textarea[editor-title]');
                        $.each($textarea, function (index, el) {
                            var $el = $(el);
                            if ($el.attr('off') === 'undefined') {
                                return;
                            }
                            new $.jqcTextarea({
                                el: $el,
                                title: $el.attr('editor-title'),
                                placeholder: $el.attr('placeholder'),
                            });
                        });
                        // 单文件上传
                        var $upload = root.find('input[upload]');
                        $.each($upload, function (index, el) {
                            var deleteTipFor = root.find('input[upload]').eq(index).attr('delete-tip-for')
                            $(el).$upload({
                                deleteTipFor: deleteTipFor
                            });
                        });
                        // 异步下拉框
                        root.find('[asyncselect]').each(function (index, el) {
                            const $this = $(el);
                            const type = $.trim($this.attr('asyncselect')).toLowerCase();
                            let id = '';
                            let adapter;
                            switch (type) {
                                case 'user':
                                    id = 'employeeGs001Completer';
                                    adapter = {
                                        value: 'id',
                                        label: function (data) {
                                            return `${data.name}-${data.deptName}`
                                        }
                                    }
                                    break;
                                case 'dept':
                                    id = 'orgGs001Completer';
                                    adapter = {
                                        value: 'id',
                                        label: function (data) {
                                            return `${data.id}-${data.name}`
                                        }
                                    }
                                    break;
                            }
                            if (id === '') {
                                console.error(`asyncselect=${type}下拉框未集成`);
                                return;
                            }
                            $this.$asyncSelect({
                                id,
                                adapter
                            });
                        });
                    });
                    if (params && params.beforeRender && typeof params.beforeRender == 'function') {
                        this._beforeRender.push(params.beforeRender);
                    }
                    this._afterRender.push(function (next) {
                        this.loading.hide();
                        next();
                    }.bind(this));
                    if (params && params.afterRender && typeof params.afterRender == 'function') {
                        this._afterRender.push(params.afterRender);
                    }
                    if (params && params.beforeDestroy && typeof params.beforeDestroy == 'function') {
                        this._beforeDestroy.push(params.beforeDestroy.bind(this));
                    }
                    _this.queryCallback = params.queryCallback ? params.queryCallback.bind(this) : null;
                    this.__scriptLoaded__ = true;
                    afterScriptLoaded.call(this);
                });
            } else {
                afterScriptLoaded.call(this);
            }

            function afterScriptLoaded() {
                // 生命周期-装载之前
                var _beforeRender = [].concat(this._beforeRender);
                if ($.App.beforeRender) {
                    _beforeRender.unshift($.App.beforeRender)
                }
                _beforeRender.push(function () {
                    $JqcLoader.importComponents(COMP_LIB_PATH, _this.components);
                    var styles = [].concat(_this.stylePath);
                    styles.forEach(style => {
                        if (style) {
                            var _path = _this.getAbsolutePath(style);
                            $JqcLoader.importCss(_path);
                        }
                    });
                    $JqcLoader.execute(function () {
                        if (_this.contextmenu) {
                            _this.__renderContextMenu();
                        }
                        if (_this.templatePath) {
                            _this.__getTemplateAndRender(data);
                        } else {
                            if (_this.dxDataGrid) {
                                _this.__renderDxDataGrid();
                            }
                            // 生命周期-渲染之后
                            _this.__afterRender(data);
                        }
                    });
                });
                queue(_beforeRender, this);                
            }
        };
        $.App.prototype.requestGet = function (api, params, loadingText, extra) {
            var _this = this;
            if (typeof loadingText == 'string') {
                this.loading.show(loadingText)
            } else if (typeof loadingText == 'boolean' && loadingText) {
                this.loading.show();
            }
            var _params;
            if (params) {
                _params = JSON.parse(JSON.stringify(params));
                var _keys = Object.keys(_params);
                _keys.forEach(_key => {
                    if (_params[_key] === '' || _params[_key] === undefined || _params[_key] === null || _params[_key] === '*') {
                        delete _params[_key];
                    }
                });
            }
            return $.ajax(Object.assign({
                url: api,
                data: _params
            }, extra)).done(res => {
                _this.loading.hide();
            });
        };
        $.App.prototype.requestPost = function (api, params, loadingText, extra) {
            var _this = this;
            if (typeof loadingText == 'string') {
                this.loading.show(loadingText)
            } else if (typeof loadingText == 'boolean' && loadingText) {
                this.loading.show();
            }
            return $.ajax(Object.assign({
                url: api,
                method: 'POST',
                data: params
            }, extra)).done(res => {
                _this.loading.hide();
            });
        }
        $.App.prototype.getFile = function (relativePath) {
            var _this = this;
            return $.ajax(_this.getAbsolutePath(relativePath) + '?v=' + +new Date());
        };
        $.App.prototype.getAbsolutePath = function (relativePath) {
            var _this = this;
            var _pathArr = this._path.split('/')
            _pathArr.pop();
            var _relativePath = relativePath.split('/');
            _relativePath.forEach(function(item) {
                if (item == '.') {
                    return;
                } else if (item == '..') {
                    _pathArr.pop();
                } else {
                    _pathArr.push(item);
                }
            });
            return _pathArr.join('/');
        };
        $.App.prototype.__getTemplateAndRender = function (data) {
            var _this = this;
            this.getFile(_this.templatePath).then(res => {
                var _fakeBox = $('<div>').append($(res));
                _this._conditionHtml = _fakeBox.find(`.${_this._config.templateClassNameMap.conditionHtmlClassName}`);
                _this._controlHtml = _fakeBox.find(`.${_this._config.templateClassNameMap.controlHtmlClassName}`);
                _this._contentHtml = _fakeBox.find(`.${_this._config.templateClassNameMap.contentHtmlClassName}`);
                // toolbar
                if (_this._conditionHtml.length || _this._controlHtml.length) {
                    _this.__renderToolBar(_this._contentHtml);
                }
                // content
                if (_this._contentHtml) {
                    _this._root.append(_this._contentHtml);
                    if (_this._contentHtml.attr('format') !== undefined) {
                        _this.mixinFormat.forEach(format => {
                            format(_this._contentHtml, 'content')
                        });
                    }
                }
                // dx表格
                if (_this.dxDataGrid) {
                    _this.__renderDxDataGrid();
                }
                _this.__afterRender(data);
            });
        };
        $.App.prototype.__renderToolBar = function (controlHtml) {
            var _this = this;
            this._toolBar = $('<div>');
            let _controlHtml = controlHtml ? controlHtml.find('.dx_Container') : null
            if (_controlHtml&&_controlHtml.length > 0) {
                _controlHtml.prepend(_this._toolBar);
            } else {
                this._root.prepend(_this._toolBar);
            }
            this.toolbar = new $.jqcFormToolBar({
                element: _this._toolBar,
                conditionHtml: _this._conditionHtml[0] || '',
                controlHtml: _this._controlHtml[0] || '',
                height: 50,
                onResize: _this.root.parents('.jqcDialogContainer').length == 0 ? function (height) {
                    if (_this.dxDataGrid && _this.getDxDataGrid()) {
                        _this.getDxDataGrid().option('height', _this.dxDataGrid.height || window.innerHeight - 110 - height);
                    }
                } : null,
                onChange: function (height) {
                    if (_this.dxDataGrid && _this.getDxDataGrid()) {
                        _this.getDxDataGrid().option('height', _this.dxDataGrid.height || window.innerHeight - 110 - height);
                    }
                }
            });
            setTimeout(function () {
                _this.mixinFormat.forEach(format => {
                    format(_this._toolBar, 'toolbar');
                });
                $.formUtil.format(_this._toolBar);
                _this._toolBar.find('.toolbar-left button.queryBtn').$debounce('click', function () {
                    var _data = $.formUtil.fetch(_this._toolBar);
                    _this.queryCallback && _this.queryCallback(_data);
                    if (_this.dxDataGrid) {
                        _this.fillDxDataGrid(_data);
                        return;
                    }
                });
            }, 0);
            var _parent = this.root.parents('.jqcTabPanel');
            $.jqcEvent.on('resize.menu', function () {
                if (_parent.is(':visible')) {
                    _this.toolbar.resize();
                }
            })
        };
        $.App.prototype.__renderDxDataGrid = function () {
            var _this = this;
            var _dxContainer = $(this._root).find('.dx_Container')
            this.$dxDataGrid = $(`<div data-dx="${this.id}">`);
            if (_dxContainer.length > 0) {
                _dxContainer.append(_this.$dxDataGrid)
            } else {
                this._root.append(_this.$dxDataGrid); // 添加dx到頁面
            }
            var config = Object.assign({
                export: {
                    fileName: this.$name,
                    excelWrapTextEnabled: true
                }
            }, this.dxDataGrid);
            this.$dx = this.createDxDataGrid(config, this.$dxDataGrid, this._contextmenu);
            // 导出事件绑定
            this.root.find('button.export-excel')
                .$debounce('click', function(e) {
                    e.stopPropagation();
                    _this.$dx.exportToExcel(false);
                });
            // 搜索事件绑定
            this.root.find('input.search')
                .keyup(function(e) {
                    e.stopPropagation();
                    var val = $(this).val();
                    _this.$dx.searchByText(val);
                });
            // 开关列筛选
            this.root.find('.column-chooser')
                .$debounce('click', function (e) {
                    const $chooser = $('.dx-datagrid-column-chooser-list');
                    if ($chooser.length && $chooser.is(':visible')) {
                        _this.$dx.hideColumnChooser();
                    } else {
                        _this.$dx.showColumnChooser();
                    }
                }, 100);
        };
        $.App.prototype.fillDxDataGridByData = function (data) {
            var _this = this;
            this.dataSource = data || [];
            this.$dx && this.$dx.option('dataSource', _this.dataSource);
        };
        // 在dx表格中插入新数据
        $.App.prototype.addFillDxDataGridByData = function (data) {
            var _this = this;
            let oldData = _this.getDxDataGrid().getDataSource()._store._array
            this.dataSource = data ? oldData.concat(data) : [];
            this.$dx && this.$dx.option('dataSource', _this.dataSource);
        };
        $.App.prototype.getDxDataGrid = function () {
            return this.$dx;
        };
        $.App.prototype.fillDxDataGrid = function (params) {
            var _this = this;
            var queryQueue = [
                function beforeQuery(next) {
                    if (_this.dxDataGrid.beforeQuery) {
                        _this.dxDataGrid.beforeQuery.bind(_this)(next, params);
                    } else {
                        next(params);
                    }
                },
                function query(next, params) {
                    _this.loading.show('加载中...').lock();
                    _this.requestGet(_this.dxDataGrid.fetchDataApi, params).then(res => {
                        if (res.code == 1) {
                            $.tip('error', '请求失败', res.msg);
                            next([]);
                        } else if (res.code == 0) {
                            next(res.result || []);
                        }
                    });
                },
                function afterQuery(next, dataSource) {
                    if (_this.dxDataGrid.afterQuery) {
                        _this.dxDataGrid.afterQuery.bind(_this)(next, dataSource);
                    } else {
                        next(dataSource);
                    }
                },
                function addListener(next, dataSource) {
                    jqcEvent.clear('filled.dxDataGrid');
                    jqcEvent.once('filled.dxDataGrid', function (that) {
                        if (that === _this) {
                            _this.$dx.refresh();
                            _this.loading.unlock().hide();
                        }
                    });
                    next(dataSource);
                },
                function done(next, dataSource) {
                    if (_this.dxDataGrid.clearFilters) {
                        _this.$dx.clearFilter();
                    }
                    _this.fillDxDataGridByData(dataSource);
                }
            ];
            queue(queryQueue, this);
        };
        $.App.prototype.__renderContextMenu = function () {
            var _this = this;
            var _options = $.extend({}, {
                width: 118,
                height: 40
            }, this.contextmenu);
            this._contextmenu = new $.jqcContextMenu(_options);
            setTimeout(function () {
                $(document).on('mousewheel.$App', function () {
                    _this._contextmenu.box && _this._contextmenu.box.remove();
                });
            }, 0);
        };
        $.App.prototype.__afterRender = function (data) {
            var _this = this;
            var _afterRender = [].concat(this._afterRender).concat(function onShow() {
                _this.__onShow__ && _this.__onShow__();
            });
            if ($.App.afterRender) {
                _afterRender.unshift($.App.afterRender);
            }
            // 带参数渲染
            _afterRender.splice(-2, 0, function (next) {
                if (data) {
                    var search = this.root.find('.toolbar-left');
                    if (search.length) {
                        $.formUtil.fill(search, data)
                    }
                }
                next(data);
                _this.$eventBus.trigger('done.render');
            });
            setTimeout(function () {
                queue(_afterRender, _this);
                _this.loading.hide();
            }, 17);
        };
        /**
         * templatePath 模板路径
         * title dialog title
         * dafaultData 默认填充数据
         * readOnly  Boolean 只读，全部设置disabled
         * dataMerge Boolean 数据合并 绑定数据与defaultData合并,有则覆盖。用于数据更新、关联。
         * api 更新 添加的api
         * disabled Array databind属性的值 ['*'] 等同于 readOnly = true;
         */
        $.App.prototype.dialog = function (params, isDialog) {
            var _this = this;
            if (params.mode == 'tab') {
                this.openTab(params);
                return;
            }
            var _dialog;
            // 没有模板
            if (!params.templatePath && !params.content) {
                throw new Error('dialog：templatePath 或 content属性未设置');
            }
            new Promise(resolve => {
                if (params.content) {
                    resolve(params.content)
                } else {
                    resolve(this.getFile(params.templatePath))
                }
            }).then(res => {
                var _template = $(res);
                var $btn = _template.find('button.done');
                var $cancel = _template.find('button.cancel');
                var $next = _template.find('button.save_and_add');
                if ((window.__PAGE_TYPE__ === 'todo' || window.__PAGE_TYPE__  === 'oaTodo') && !isDialog) {  
                    _dialog = $('<div>').css({
                        width: params.width || 1080
                    });
                    var $dialogTitle = $('.dialog_title');
                    $dialogTitle.text(params.title);
                    $dialogTitle.css('width', params.width || 1080);
                    $('.dialog_body').append(_dialog);
                    _dialog.append(_template);
                    _dialog.close = function (time = 3000) {
                        _this.loading.show().lock();
                        setTimeout(() => {
                            window.close();
                        }, time);
                    }
                    var actionId = getQueryString('actionId');
                    var recordId = getQueryString('recordId');
                    if (actionId && recordId && window.__PAGE_TYPE__  !== 'oaTodo') {
                        if (!$.App.$actions) {
                            throw new Error('$.App未启用actions插件');
                        }
                        var actionConfig = $.App.$actions[actionId];
                        if ($.type(actionConfig) !== 'object') {
                            throw new Error(`$.App actions插件: ${actionId}不存在`);
                        }
                        $.ajax({
                            url: 'todo/currentProcessor.wf',
                            data: { recordId: recordId }
                        }).then(res => {
                            if (!res.result || !res.result.processorList || !res.result.processorList.length) {
                                return;
                            }
                            var $nextUsers = $('<p>');
                            res.result.processorList.forEach(item => {
                                var $user = $('<span>').text(item.displayName);
                                $user.data('userInfo', item);
                                $nextUsers.append($user);
                            })
                            $dialogTitle.append($nextUsers)
                            $dialogTitle.on('click', 'span', function () {
                                var userInfo = $(this).data('userInfo');
                                var $content = $('<div>').addClass('dialog_user_info_container');
                                var $name = $('<p>').attr('data-before', '姓名：').text(userInfo.displayName + '(' + userInfo.userID + ')');
                                var $phone = $('<p>').attr('data-before', '手机号：').text(userInfo.mobile);
                                var $email = $('<p>').attr('data-before', '邮箱：').text(userInfo.mail);
                                var $tel = $('<p>').attr('data-before', '电话：').text(userInfo.telephoneNum);
                                $content.append($name, $phone, $tel, $email)
                                var dialog = new $.jqcDialog({
                                    title: '待办人信息',
                                    content: $content,
                                    width: 400
                                });
                                dialog.open()
                            })
                        });
                    }
                } else {
                    _dialog = new $.jqcDialog({
                        title: params.title || '',
                        content: _template,
                        width: params.width || 1080,
                        position: params.position || 'auto',
                        onClose: params.onClose,
                        beforeClose: params.beforeClose,
                        afterClose: function () {
                            params.afterClose && params.afterClose();
                            $.each(_template.find('input'), function (index, el) {
                                if (el.jqcSelectBox) {
                                    el.jqcSelectBox.destroy();
                                }
                                if ($(el).data('xdsoft_datetimepicker')) {
                                    $(el).datetimepicker('destroy');
                                }
                            })
                            if (params.todoId) {
                                _dialog.titleBar.find('.jqcDialogTitle p').remove();
                                _dialog.titleBar.find('.jqcDialogTitle').off('click.todo');
                            }
                        }
                    });
                    // 显示待办人
                    if (params.todoId) {
                        $.ajax({
                            url: 'todo/currentProcessor.wf',
                            data: { recordId: params.todoId }
                        }).then(res => {
                            if (!res.result || !res.result.processorList || !res.result.processorList.length) {
                                return;
                            }
                            var $nextUsers = $('<p>');
                            res.result.processorList.forEach(item => {
                                var $user = $('<span>').text(item.displayName);
                                $user.data('userInfo', item);
                                $nextUsers.append($user);
                            })
                            var $dialogTitle = _dialog.titleBar.find('.jqcDialogTitle').append($nextUsers);
                            $dialogTitle.on('click.todo', 'span', function () {
                                var userInfo = $(this).data('userInfo');
                                var $content = $('<div>').addClass('dialog_user_info_container');
                                var $name = $('<p>').attr('data-before', '姓名：').text(userInfo.displayName + '(' + userInfo.userID + ')');
                                var $phone = $('<p>').attr('data-before', '手机号：').text(userInfo.mobile);
                                var $email = $('<p>').attr('data-before', '邮箱：').text(userInfo.mail);
                                var $tel = $('<p>').attr('data-before', '电话：').text(userInfo.telephoneNum);
                                $content.append($name, $phone, $tel, $email)
                                var dialog = new $.jqcDialog({
                                    title: '待办人信息',
                                    content: $content,
                                    width: 400
                                });
                                dialog.open()
                            })
                        })
                    }
                }
                if (Array.isArray(params.disabled)) {
                    if (params.disabled.length === 1 && params.disabled[0] === '*') {
                        _template.find('[databind]').prop('disabled', true);
                    } else {
                        params.disabled.forEach(item => {
                            _template.find(`[databind=${item}]`).prop('disabled', true);
                            _template.find(`[gdatabind=${item}]`).prop('disabled', true);
                        });
                    }
                }
                _this.mixinFormat.forEach(format => {
                    format(_template, 'dialog');
                });
                setTimeout(function () {
                    _dialog.content && _dialog.content.scrollTop(0);
                    params.afterRender && params.afterRender(_template, _dialog);
                    if (params.defaultData) {
                        $.formUtil.fill(_template, params.defaultData);
                    } else {
                        $.formUtil.format(_template);
                    }
                    // _template.find('input[upload][disabled]').each(function (index, el) {
                    //     var $el = $(el);
                    //     $el.siblings('div').remove();
                    //     var $before;
                    //     if (el.value) {
                    //         $before = $('<a>').text('下载').addClass('text_action').attr({
                    //             'href': './file/download/' + el.value,
                    //             'target': '_blank'
                    //         });
                    //     } else {
                    //         $before = $('<p>').text('无');
                    //     }
                    //     $before.css({
                    //         width: 200
                    //     });
                    //     $el.before($before);
                    // });
                    _dialog.filled = true;
                    params.afterFill && params.afterFill(_template, _dialog, params.defaultData);
                }, 10);
                if (params.readOnly) {
                    _template.find('[databind]').prop('disabled', true);
                    _template.find('[gdatabind]').prop('disabled', true);
                    _template.find('button').hide();
                }
                _dialog.open && _dialog.open();
                if ($next.length == 1) {
                    $next.$debounce('click', function (e) {
                        clickCallback.call(this, e, 'next');
                    })
                }
                $btn.$debounce('click', clickCallback);
                _template.find('button[action-id]').$debounce('click', function (e) {
                   var $this = $(this);
                   var actionId = $this.attr('action-id');
                   clickCallback.call(this, e, actionId);
                });
                function clickCallback(e, type) {
                    var $this = $(this);
                    var loadingTxt = $this.attr('loading') || true;
                    if ($this.attr('unloading') === '' || $this.attr('unloading')) {
                        loadingTxt = false;
                    }
                    setTimeout(function () {
                        // 提交操作队列
                        var submit_queue = [];
                        // 获取数据之前执行
                        params.beforeFetchData && submit_queue.push(function (next) {
                            params.beforeFetchData(_template, next, _dialog, type);
                        });
                        // 获取数据
                        submit_queue.push(function fetchData(next) {
                            var _data = $.formUtil.fetch(_template);
                            if (!params.isInsert) {
                                _data = Object.assign({}, params.defaultData, _data);
                            }
                            if (params.check && !params.check(_data)) {
                                return;
                            }
                            next(_data);
                        });
                        // 提交数据之前执行
                        params.beforeSubmit && submit_queue.push(function (next, _data) {
                            params.beforeSubmit(_data, _template, next, _dialog, type);
                        });
                        // 提交数据
                        submit_queue.push(function (next, _data, header, url) {
                            submit(next, _data, header, url)
                        });
                        // 提交数据之后执行
                        params.afterSubmit && submit_queue.push(function (next, res, success, failded) {
                            params.afterSubmit(res, success, failded, _template, _dialog, type);
                        });
                        queue(submit_queue);
                        function submit(next, _data, header, url) {
                            _this.requestPost((url || params.api), _data, loadingTxt, (header || {})).then(res => {
                                // 异步回调
                                function success() {
                                    if (params.updateCache && _this.updateCache) {
                                        _this.updateCache(params.updateCache);
                                    }
                                    $.jqcNotification({
                                        type: 'success',
                                        title: '操作成功'
                                    });
                                    _this.triggerQuery(params.fillParams);
                                    // 新增下一个
                                    if (type == 'next') {
                                        _template.find('input,textarea').val('');
                                        $.formUtil.fill(_template, params.defaultData);
                                        _dialog.filled = true;
                                        _template.find('.jqcFormUtil_invalid').removeClass('jqcFormUtil_invalid');
                                        params.afterFill && params.afterFill(_template, _dialog, params.defaultData);
                                        if (Array.isArray(params.disabled)) {
                                            if (params.disabled.length === 1 && params.disabled[0] === '*') {
                                                _template.find('[databind]').attr('disabled', 'disabled');
                                            } else {
                                                params.disabled.forEach(item => {
                                                    _template.find(`[databind=${item}]`).attr('disabled', 'disabled');
                                                });
                                            }
                                        }
                                    } else {
                                        if (window.__PAGE_TYPE__ === 'todo' || window.__PAGE_TYPE__ === 'oaTodo') {
                                            window.close();
                                        } else {
                                            _dialog.close();
                                        }
                                    }
                                }
                                function failed() {
                                    $.jqcNotification({
                                        type: 'error',
                                        title: '操作失败。',
                                        content: res.msg
                                    });
                                }
                                if (params.afterSubmit) {
                                    next(res, success, failed);
                                    return;
                                }
                                // 默认同步代码
                                if (res.code == 0) {
                                    if (type == 'next') {
                                        _template.find('input').val('');
                                        $.formUtil.fill(_template, params.defaultData);
                                        _dialog.filled = true;
                                        params.afterFill && params.afterFill(_template, _dialog, params.defaultData);
                                        if (Array.isArray(params.disabled)) {
                                            if (params.disabled.length === 1 && params.disabled[0] === '*') {
                                                _template.find('[databind]').attr('disabled', 'disabled');
                                            } else {
                                                params.disabled.forEach(item => {
                                                    _template.find(`[databind=${item}]`).attr('disabled', 'disabled');
                                                });
                                            }
                                        }
                                    } else {
                                        if (window.__PAGE_TYPE__ === 'todo' || window.__PAGE_TYPE__ === 'oaTodo') {
                                            window.close();
                                        } else {
                                            _dialog.close();
                                        }
                                    }
                                    _this.triggerQuery(params.fillParams);
                                    if (params.success) {
                                        params.success(res, _dialog);
                                    } else {
                                        var config = {
                                            type: 'success',
                                            title: '操作成功'
                                        };
                                        if (res.msg != undefined) {
                                            config.content = res.msg;
                                        }
                                        $.jqcNotification(config);
                                    }
                                    if (params.updateCache && _this.updateCache) {
                                        _this.updateCache(params.updateCache);
                                    }
                                } else if (res.code == 1) {
                                    if (params.failed) {
                                        params.failed(res, _dialog);
                                    } else {
                                        $.jqcNotification({
                                            type: 'error',
                                            title: '操作失败。',
                                            content: res.msg
                                        });
                                    }
                                }
                            });
                        }
                    }, 20);
                }

                $cancel.$debounce('click', function (e) {
                    if (window.__PAGE_TYPE__ === 'todo' || window.__PAGE_TYPE__ === 'oaTodo') {
                        window.close();
                    } else {
                        _dialog.close(0);
                    }
                });
            });
        };

        $.App.prototype.openTab = function (params) {
            var _this = this;
            var id = params.tabId;
            if (!id) {
                throw new Error('tab页缺少id!');
            }
            if (jqcTab.index.has(id)) {
                jqcTab.add({
                    id: id
                });
                return;
            }
            // 没有模板
            if (!params.templatePath) {
                return;
            }
            this.getFile(params.templatePath).then(res => {
                var _template = $(res);
                var _content = $('<div>').css({
                    'width': params.width,
                    'margin': 'auto'
                }).append(_template);
                var $btn = _template.find('button.done');
                var $next = _template.find('button.save_and_add');
                jqcTab.add({
                    id,
                    title: params.title || '',
                    content: _content,
                    beforeDestroy: function () {
                        $.each(_template.find('input'), function (index, el) {
                            if (el.jqcSelectBox) {
                                el.jqcSelectBox.destroy();
                            }
                            if ($(el).data('xdsoft_datetimepicker')) {
                                $(el).datetimepicker('destroy');
                            }
                        })
                        if (jqcTab.index.get(id).isActive) {
                            setTimeout(function () {
                                var parent = jqcTab.container.find('span[tabid="' + _this._id + '"]');
                                if (parent.length) {
                                    parent.trigger('click');
                                }
                            }, 16)
                        }
                    }
                });
                var _dialog = {
                    close: function () {
                        jqcTab.container.find('span[closeid="' + id + '"]').trigger('click');
                        params.afterClose && params.afterClose();
                        setTimeout(function () {
                            try {
                                _this.getDxDataGrid().refresh(true);
                            } catch (error) {

                            }
                        }, 1000)
                    }
                };

                if (Array.isArray(params.disabled)) {
                    if (params.disabled.length === 1 && params.disabled[0] === '*') {
                        _template.find('[databind]').attr('disabled', 'disabled');
                    } else {
                        params.disabled.forEach(item => {
                            _template.find(`[databind=${item}]`).attr('disabled', 'disabled');
                        });
                    }
                }
                _this.mixinFormat.forEach(format => {
                    format(_template, 'content');
                });
                setTimeout(function () {
                    params.afterRender && params.afterRender(_template, _dialog);
                    if (params.defaultData) {
                        $.formUtil.fill(_template, params.defaultData);
                    } else {
                        $.formUtil.format(_template);
                    }
                }, 10);
                if (params.readOnly) {
                    _template.find('[databind]').attr('disabled', 'disabled');
                    _template.find('button').hide();
                }
                if ($next.length == 1) {
                    $next.$debounce('click', function () {
                        _this.loading.lock(500);
                        $btn.length == 1 && $btn.trigger('click', 'next');
                    })
                }
                $btn.$debounce('click', function (e, type) {
                    var loadingTxt = $(this).attr('loading');
                    if (loadingTxt == undefined) {
                        loadingTxt = false
                    } else {
                        loadingTxt = loadingTxt || true;
                    }
                    setTimeout(function () {
                        // 提交操作队列
                        var submit_queue = [];
                        // 获取数据之前执行
                        params.beforeFetchData && submit_queue.push(function (next) {
                            params.beforeFetchData(_template, next, _dialog, type);
                        });
                        // 获取数据
                        submit_queue.push(function fetchData(next) {
                            var _data = $.formUtil.fetch(_template);
                            if (!params.isInsert) {
                                _data = Object.assign({}, params.defaultData, _data);
                            }
                            if (params.check && !params.check(_data)) {
                                return;
                            }
                            next(_data);
                        });
                        // 提交数据之前执行
                        params.beforeSubmit && submit_queue.push(function (next, _data) {
                            params.beforeSubmit(_data, _template, next, _dialog, type);
                        });
                        // 提交数据
                        submit_queue.push(function (next, _data) {
                            submit(_data, next)
                        });
                        // 提交数据之后执行
                        params.afterSubmit && submit_queue.push(function (next, res, success, failded) {
                            params.afterSubmit(res, success, failded, _template, _dialog, type);
                        });
                        queue(submit_queue);
                        function submit(_data, next) {
                            _this.requestPost(params.api, _data, loadingTxt).then(res => {
                                // 异步回调
                                function success() {
                                    if (params.updateCache && _this.updateCache) {
                                        _this.updateCache(params.updateCache);
                                    }
                                    $.jqcNotification({
                                        type: 'success',
                                        title: '操作成功'
                                    });
                                    _this.triggerQuery(params.fillParams);
                                    // 新增下一个
                                    if (type == 'next') {
                                        _template.find('input').val('');
                                        $.formUtil.fill(_template, params.defaultData);
                                        if (Array.isArray(params.disabled)) {
                                            if (params.disabled.length === 1 && params.disabled[0] === '*') {
                                                _template.find('[databind]').attr('disabled', 'disabled');
                                            } else {
                                                params.disabled.forEach(item => {
                                                    _template.find(`[databind=${item}]`).attr('disabled', 'disabled');
                                                });
                                            }
                                        }
                                    } else {
                                        _dialog.close();
                                    }
                                }
                                function failed() {
                                    $.jqcNotification({
                                        type: 'error',
                                        title: '操作失败。',
                                        content: res.msg
                                    });
                                }
                                if (params.afterSubmit) {
                                    next(res, success, failed);
                                    return;
                                }
                                // 默认同步代码
                                if (res.code == 0) {
                                    if (type == 'next') {
                                        _template.find('input').val('');
                                        $.formUtil.fill(_template, params.defaultData);
                                        if (Array.isArray(params.disabled)) {
                                            if (params.disabled.length === 1 && params.disabled[0] === '*') {
                                                _template.find('[databind]').attr('disabled', 'disabled');
                                            } else {
                                                params.disabled.forEach(item => {
                                                    _template.find(`[databind=${item}]`).attr('disabled', 'disabled');
                                                });
                                            }
                                        }
                                    } else {
                                        _dialog.close();
                                    }
                                    _this.triggerQuery(params.fillParams);
                                    if (params.success) {
                                        params.success(res, _dialog);
                                    } else {
                                        var config = {
                                            type: 'success',
                                            title: '操作成功'
                                        };
                                        if (res.msg != undefined) {
                                            config.content = res.msg;
                                        }
                                        $.jqcNotification(config);
                                    }
                                    if (params.updateCache && _this.updateCache) {
                                        _this.updateCache(params.updateCache);
                                    }
                                } else if (res.code == 1) {
                                    if (params.failed) {
                                        params.failed(res, _dialog);
                                    } else {
                                        $.jqcNotification({
                                            type: 'error',
                                            title: '操作失败。',
                                            content: res.msg
                                        });
                                    }
                                }
                            });
                        }
                    }, 20);
                })
            });
        };
        $.App.prototype.triggerQuery = function (params) {
            // OP代办列表打开页面不触发查询操作
            if (window.__PAGE_TYPE__ === 'todo' || window.__PAGE_TYPE__ === 'oaTodo') {
                return;
            }
            var queryBtn = this.root.find('.toolbar-left button.queryBtn');
            if (queryBtn.length) {
                queryBtn.trigger('click');
            } else if (this.dxDataGrid){
                this.fillDxDataGrid(params);
            } else {
                // nothing
            }
        };
        $.App.prototype.delete = function (params) {
            var _this = this;
            $.jqcConfirm({
                title: params.title,
                content: params.content,
                onConfirm: function () {
                    if (!params.api || !params.data) {
                        throw new Error('delete方法缺少“api”或“data”属性！');
                    }
                    _this.requestPost(params.api, params.data, '删除中...').then(res => {
                        if (params.afterSubmit) {
                            params.afterSubmit(res, success, error);
                            return;
                        }
                        // 兼容老代码
                        if (res.code == 0) {
                            if (params.success) {
                                params.success(res);
                                if (params.updateCache && _this.updateCache) {
                                    _this.updateCache(params.updateCache);
                                }
                            } else {
                                success();
                            }
                        } else if (res.code == 1) {
                            error(res.msg);
                        }
                        function success (msg) {
                            $.jqcNotification({
                                type: 'success',
                                title: '删除成功',
                                content: msg
                            });
                            if (params.updateCache && _this.updateCache) {
                                _this.updateCache(params.updateCache);
                            }
                            _this.triggerQuery(params.fillParams);
                        }

                        function error(msg) {
                            $.jqcNotification({
                                type: 'error',
                                title: '删除失败',
                                content: msg
                            });
                        }
                    });    
                }
            });
        };
        $.App.prototype.createSelectBox = function (params) {
            var _this = this;
            params.element[0].jqcSelectBox && params.element[0].jqcSelectBox.destroy();
            var config = {
                dataName: '',
                supportFuzzyMatch: true,
                supportPinYin: true,
                pinyinParser: _this.pinyinParser,
                width: 200,
                onSelect: function (data) {
                    params.element && params.element.trigger('change', data);
                }
            };
            Object.assign(config, params);
            params.element[0].jqcSelectBox = new $.jqcSelectBox(config);
        };
        // 打开子页面
        $.App.prototype.openChildPage = function ({app, title, width, height, data}) {
            var _this = this;
            if (!app || !app.mount) {
                throw new Error('openChildPage expect a $.App instance!');
            }
            title = title || '';
            width = width || 1080;
            height = height || 470;
            // 设置默认dx高度
            if (app.__options__.dxDataGrid && !app.__options__.dxDataGrid.height) {
                app.__options__.dxDataGrid.height = height 
            }
            var $root = $('<div>').attr('data-path', _this._path).addClass('jqcTabPanel').css('height', height + 80);
            this.dialog({
                title,
                width,
                content: $root,
                beforeClose: function () {
                    var _beforeDestroy = [].concat(app._beforeDestroy);
                    if ($.App.beforeDestroy) {
                        _beforeDestroy.unshift($.App.beforeDestroy);
                    }
                    queue(_beforeDestroy, app);
                },
                afterRender: function (root, dialog) {
                    app.$dialog = dialog;
                    app.mount($root, data);
                }
            })
        }
        // 打开页面
        $.App.prototype.openPage = function (formUrl, title, data, isReload) {
            var _this = this;
            var needReload = (isReload === undefined || isReload) ? true : false;
            $.addForm({
                text: title,
                url: formUrl,
                data: data
            }, jqcTab, needReload, _this._id);
        }
        // 在当前app页面指定容器内新建子app
        $.App.prototype.createChildApp = function (app, $container, data) {
            $container.attr('data-path', this._path);
            app.mount($container, data);
        }
        $.App.prototype.createDxDataGrid = function (config, $container, contextmenu) {
            var _this = this;
            var defaultConfig = {
                dataSource: [],
                allowColumnResizing: true,
                height: window.innerHeight - 170,
                width: '100%',
                filterRow: {
                    visible: true,
                },
                headerFilter: {
                    visible: true,
                },
                hoverStateEnabled: true,
                rowAlternationEnabled: true,
                grouping: {
                    expandMode: 'rowClick'
                },
                columnAutoWidth: true,
                scrolling: {
                    mode: 'infinite',
                    // showScrollbar: 'always'
                },
                paging: {
                    pageSize: 20
                },
                showRowLines: true,
                showBorders: true,
                loadPanel: {
                    enabled: false
                },
                onContentReady: function () {
                    jqcEvent.emit('filled.dxDataGrid', _this);
                }
            };
            var firstColumn = config.hideOrder ? [] : [{
                caption: '序号',
                fixed: true,
                width: 70,
                cssClass: 'bgf5f6fa',
                alignment: 'center',
                cellTemplate: function(box, data) {
                    var index = data.rowIndex + 1;
                    var total = data.component.option('dataSource').length;
                    box.text(index);
                    box.attr('title', `第${index}条/共${total}条`);
                },
                allowExporting: false
            }];
            var has$columns = typeof this.$columns === 'function';
            if (typeof config.columns === 'string') {
                if (has$columns) {
                    config.columns = this.$columns(config.columns, this);
                } else {
                    $.tip('error', '$.App未发现columns插件!');
                    return;
                }
            }
            var columns = firstColumn.concat(config.columns || []).map(item => {
                var _item = {};
                var invisible = [].concat(config.invisible).find(i => i === item.dataField);
                if (invisible) {
                    _item.visible = false;
                }
                var _temp = {};
                var dataField = item.dataField;
                if (!item.hasOwnProperty('dataType') && !item.lookup) {
                    item.dataType = 'string';
                }
                if (item.dataType === 'date') {     // 日期
                    _temp = {
                        format: $.timeFormat(),
                        width: 150,
                        calculateCellValue: function (rowData) {
                            var value = rowData[dataField];
                            if ($.trim(value) === '') {
                                return undefined;
                            }
                            if (!isNaN(value)) {
                                return new Date(+value);
                            }
                            if (typeof value === 'string') {
                                return new Date(value);
                            }
                        }
                    }
                } else if (item.dataType === 'datetime') {      // 时间
                    _temp = {
                        format: $.timeFormat('yyyy-MM-dd HH:mm:ss'),
                        width: 180,
                        calculateCellValue: function (rowData) {
                            var value = rowData[dataField];
                            if ($.trim(value) === '') {
                                return undefined;
                            }
                            if (!isNaN(value)) {
                                return new Date(+value);
                            }
                            if (typeof value === 'string') {
                                return new Date(value);
                            }
                        }
                    }
                } else if (item.dataType === 'boolean') {       // 布尔   0：否， 1：是
                    item.dataType = 'string';
                    _temp = {
                        calculateCellValue: function (rowData) {
                            if (rowData[dataField] === '' || rowData[dataField] === undefined) {
                                return '';
                            }
                            return rowData[dataField] ? '是' : '否';
                        }
                    }
                }
                if (!item.selectedFilterOperation) {
                    item.selectedFilterOperation = item.dataType === 'string' ? 'contains' : '=';
                }
                Object.assign(_item, _temp, item);
                return _item;
            });
            // 添加操作按钮组
            if ($.jqcToolkit.rawType(config.btns) === 'Array') {
                var width;
                if (config.btnsWidth) {
                    width = config.btnsWidth;
                } else {
                    width = config.btns.map(i => i.text).join('').length * 16 + config.btns.length * 50;
                }
                columns.push({
                    caption: '操作',
                    fixed: true,
                    alignment: 'center',
                    allowExporting: false,
                    width,
                    fixedPosition: 'right',
                    allowSearch: false,
                    cssClass: 'dx_btns',
                    cellTemplate: function (cell, e) {
                        config.btns.forEach(btn => {
                            if (!btn.valid || btn.valid(e.key)) {
                                var text = typeof btn.text === 'function' ? btn.text(e.key, _this) : btn.text;
                                var $btn = $('<span>').text(text).data('rowData', e.key).$debounce('click', function (event) {
                                    event.stopPropagation();
                                    $('body').find('.jqcContextMenu').remove();
                                    btn.action(e.key, event, _this);
                                });
                                cell.append($btn);
                            }
                        });
                    }
                });
            }
            var dxConfig = Object.assign({}, defaultConfig, config, { columns });
            if (config.hideOrder) {
                dxConfig.scrolling = {
                    mode: 'virtual',
                    // showScrollbar: 'always'
                }
            }
            if (!isNaN(config.pageSize)) {
                dxConfig.paging.pageSize = config.pageSize;
            }
            if (contextmenu) {
                dxConfig.onContextMenuPreparingExt ? dxConfig.onContextMenuPreparing = function (e) {
                    _this.contextmenu = contextmenu
                    _this.__renderContextMenu()
                    dxConfig.onContextMenuPreparingExt.call(_this, e, _this._contextmenu);
                } : dxConfig.onContextMenuPreparing = function (e) {
                    e.jQueryEvent.preventDefault();
                    if (!e.row || e.row.rowType !== 'data') {
                        return;
                    }
                    contextmenu.show(e.row.data)
                }
            }
            if (!dxConfig.rowAlternationEnabled) {
                dxConfig.elementAttr = {
                    class: 'custom-row-style'
                }
            }
            var _onRowClick = dxConfig.onRowClick;
            var _onRowDblClick = dxConfig.onRowDblClick;
            var clickCount = 0;
            var timer = null;
            dxConfig.onRowClick = function (e) {
                clickCount ++;
                clearTimeout(timer);
                timer = setTimeout(() => {
                   if (clickCount < 2) {
                       _onRowClick && _onRowClick(e);
                   } else {
                       _onRowDblClick && _onRowDblClick(e);
                   }
                   clickCount = 0;
                }, 300);
            }
            var _onCellClick = dxConfig.onCellClick;
            var _onCellDblClick = dxConfig.onCellDblClick;
            var cellClickCount = 0;
            var cellClickTimer = null;
            dxConfig.onCellClick = function (e) {
                cellClickCount ++;
                clearTimeout(cellClickTimer);
                cellClickTimer = setTimeout(() => {
                   if (cellClickCount < 2) {
                       _onCellClick && _onCellClick(e);
                   } else {
                       _onCellDblClick && _onCellDblClick(e);
                   }
                   cellClickCount = 0;
                }, 300);
            }
            if (_onRowClick || _onRowDblClick || _onCellClick || _onCellDblClick) {
                dxConfig.elementAttr = {
                    'class': 'pointer'
                }
            }
            $container.dxDataGrid(dxConfig);
            return $container.dxDataGrid('instance');
        }
        $.App.prototype.exportToExcel = function (config, callback) {
            if (!arguments.length) {
                this.$dx && this.$dx.exportToExcel();
                return;
            }
            const _this = this;
            if (!config.alwaysExport && (!config.dataSource || !config.dataSource.length)) {
                $.tip('warn', '无法导出空数据');
                return;
            }
            this.loading.show('导出中...');
            const $container = $('<div>').css({
                width: config.width || 1080,
                position: 'absolute',
                bottom: -99999,
                right: -99999
            });
            $('body').append($container)
            const _config = Object.assign({}, config, {
                export: {
                    fileName: config.fileName,
                    excelWrapTextEnabled: true
                },
                onExported: function () {
                    callback && callback();
                    $container.remove();
                    _this.loading.hide();
                }
            });
            const instance = this.createDxDataGrid(_config, $container)
            setTimeout(() => {
                instance.exportToExcel();
            }, 1000)
        }

        $.App.plugin = function (pluginName, fn) {
            this['$' + pluginName] = fn;
        }
    });