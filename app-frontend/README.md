# App前端框架

**依赖于node.js**

### 安装打包依赖（仅需要安装一次）

```bash
npm i
```

### 打包编译压缩

直接执行**syncAndCompressFiles.bat**脚本

### 主要文件夹

```
src                      开发代码

libs                     本地测试代码

libs-compressed          生产部署代码
```