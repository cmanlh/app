var BrowserMatch = {
    isIE: function () {
        var ua = navigator.userAgent.toLowerCase();
        return ua.indexOf('trident') > -1;
    },
    getChrome: function () {
        if (this.getOS() != 'win') {
            return '';
        }
        var digits = this.getDigits();
        if (digits == '64') {
            return 'http://ms.orientsec.com.cn/cdn/chrome64.msi'
        } else {
            return 'http://ms.orientsec.com.cn/cdn/chrome32.msi'
        }
    },
    getOS: function () { //判断所处操作系统
        var isWin = (navigator.platform == "Win32") || (navigator.platform == "Win64") || (navigator.platform == "wow64");
        if (isWin) return 'win';
        var isMac = (navigator.platform == "Mac68K") || (navigator.platform == "MacPPC") || (navigator.platform == "Macintosh") || (navigator.platform == "MacIntel");
        if (isMac) return "mac";
    },
    getDigits: function () { //判断当前操作系统的版本号 
        var sUserAgent = navigator.userAgent.toLowerCase();
        var is64 = sUserAgent.indexOf("win64") > -1 || sUserAgent.indexOf("wow64") > -1;
        if (is64) {
            return "64";
        } else {
            return "32";
        }
    },
    isChrome: function () {
        var reg =  /(chrome)\/([\d.]+)/;
        var ua = navigator.userAgent.toLowerCase();
        return reg.test(ua);
    },
    getChromeVersion: function () {
        var reg = /chrome\/\d+/;
        var ua = navigator.userAgent.toLowerCase();
        var result = ua.match(reg);
        if (result && result.length) {
            return +(result[0].substr(7));
        }
        return undefined;
    },
    downloadChrome: function () {
        var url = BrowserMatch.getChrome();
        var scripts = document.scripts;
        var baseSrc = scripts[scripts.length - 1].src.split('/');
        baseSrc.splice(-2, 2);
        var img_src = baseSrc.concat(['css', 'images', 'browser.png']).join('/');
        var icon_src = baseSrc.concat(['css', 'images', 'icon_download.png']).join('/');
        var html = '<div style="position: fixed;width: 100%;height: 100%;background: #fff;top: 0;left: 0;z-index: 999999;"><div style="height: 220px;width: 750px;position: absolute;top: 50%;margin-top: -110px;left: 50%;margin-left: -375px;"> <div style="float: left;height: 220px;width: 420px;"><img style="width:420px;height:220px;display:block;" src="'+img_src+'" /></div> <div style="width: 270px;float: right;"><p style="line-height: 60px;font-size: 40px;margin: 16px 0 10px;">浏览器不兼容</p><p style="font-size: 18px;line-height: 28px;color: #666;margin: 0 0 30px;">请您点击下方下载推荐的浏览器，非常感谢您的配合！</p><a href="'+url+'" style="display: block;width: 176px;height: 30px;border: 1px solid #bbb;border-radius: 2px;"><div style="float: left;width: 16px;height: 16px;margin: 7px 10px 7px 14px;"><img src="'+icon_src+'" style="border:none;display:block;"/></div><p style="float: left;line-height: 30px;font-size: 14px;color: #444;margin: 0;">下载Chrome浏览器</p></a></div></div></div>';
        document.body.innerHTML = html;
    },
    isSupportedES6: function () {
        return typeof Promise !== 'undefined' &&
            typeof Map !== 'undefined' &&
            typeof Set !== 'undefined' &&
            typeof Object.assign !== 'undefined' &&
            typeof Object.keys !== 'undefined' &&
            typeof Object.values !== 'undefined' &&
            typeof Object.defineProperty !== 'undefined' &&
            typeof Object.defineProperty !== 'undefined' &&
            typeof isNaN !== 'undefined' &&
            typeof Array.prototype.filter !== 'undefined' &&
            typeof Array.prototype.find !== 'undefined' &&
            typeof Array.prototype.forEach !== 'undefined' &&
            typeof Array.prototype.map !== 'undefined' &&
            typeof Function.prototype.bind !== 'undefined' &&
            typeof String.prototype.trim !== 'undefined' &&
            typeof Symbol !== 'undefined'
    }
};
if (Object.values === undefined) {
    Object.values = function (object) {
        if (object === undefined) {
            throw new Error('Object.values:参数不能为空');
        }
        var temp = [];
        for (var key in object) {
            if (object.hasOwnProperty(key)) {
                var element = object[key];
                temp.push(element);
            }
        }
        return temp;
    }
}
/**
 * 判断条件
 */
if (!BrowserMatch.isSupportedES6()) {
    BrowserMatch.downloadChrome();
    throw new Error('浏览器不兼容，请下载chrome浏览器');
}