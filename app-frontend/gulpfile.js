const gulp = require('gulp')
const del = require('del')
const spawn = require('child_process').spawn
const uglify = require('gulp-uglify')
const cssmin = require('gulp-cssmin')
const babel = require('gulp-babel')
const path = require('path')

const ignoreJS = [
    'libs/**/devexpress/*.js', 
    'libs/**/jquery/*.js'
];
const ignore = ignoreJS.map(i => '!' + i);

// js转es5并压缩
gulp.task('mini:js', function (done) {
  return gulp.src(['libs/com/**/*.js', '!libs/com/**/jqc/**/*.js'], {base: 'libs/'})
  // return gulp.src(['libs/**/app/js/*.js', '!libs/ops/*'], {base: 'libs/'})
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(uglify())
    .pipe(gulp.dest('libs-compressed'));
})
// css压缩
gulp.task('mini:css', function () {
  return gulp.src(['libs/com/**/*.css', '!libs/com/**/jqc/**/*.css'], {base: 'libs/'})
    .pipe(cssmin())
    .pipe(gulp.dest('libs-compressed'));
})

// js、css并行压缩
gulp.task('mini', gulp.parallel('mini:js', 'mini:css', (done) => {
  done()
  console.log('代码压缩中。。。')
}))

// 删除jqc-release内的文件
gulp.task('clean', function (done) {
  del([
    'libs-compressed'
  ]).then(res => {
    done()
  })
})

// 复制其它文件
gulp.task('copy', function () {
  // return gulp.src(['libs/**/*', '!libs/**/jqc/**', '!libs/ops/**', '!libs/**/app/*.js', '!libs/**/app/*.css'], {base: 'libs/'})
  return gulp.src(['libs/com/**/*', '!libs/com/**/jqc/**', '!libs/com/**/*.js', '!libs/com/**/*.css'], {base: 'libs/'})
    .pipe(gulp.dest('libs-compressed'))
})

// 不处理第三方js代码
gulp.task('copy:big-js', function () {
  return gulp.src(ignoreJS, {base: 'com/'})
    .pipe(gulp.dest('docs/jqc-release/com'))
})

gulp.task('default', gulp.series('clean', 'copy', 'mini', function (done) {
    // gulp.task('default', gulp.series('clean', 'copy', 'copy:big-js', 'mini', function (done) {
  done()
  console.log('压缩完成！')
  console.log('\n\n存放至   ' + path.join(__dirname, 'libs-compressed\n\n'))
}))
