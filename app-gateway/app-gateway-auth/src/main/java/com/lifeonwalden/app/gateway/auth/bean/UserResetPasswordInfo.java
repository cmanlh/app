package com.lifeonwalden.app.gateway.auth.bean;

import java.io.Serializable;

public class UserResetPasswordInfo implements Serializable {
    private static final long serialVersionUID = -7639432466425724596L;

    private String clientSign;

    private String userId;

    private String oldPassword;

    private String newPassword;

    public String getClientSign() {
        return clientSign;
    }

    public UserResetPasswordInfo setClientSign(String clientSign) {
        this.clientSign = clientSign;

        return this;
    }

    public String getUserId() {
        return userId;
    }

    public UserResetPasswordInfo setUserId(String userId) {
        this.userId = userId;

        return this;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public UserResetPasswordInfo setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
        return this;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public UserResetPasswordInfo setNewPassword(String newPassword) {
        this.newPassword = newPassword;
        return this;
    }
}
