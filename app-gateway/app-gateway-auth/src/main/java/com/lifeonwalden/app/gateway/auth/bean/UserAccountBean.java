package com.lifeonwalden.app.gateway.auth.bean;

import java.io.Serializable;

public class UserAccountBean implements Serializable {
    private static final long serialVersionUID = -8377986593810459551L;

    private String userId;

    private String pwd;

    private String newClientSign;

    private String oldClientSign;

    private boolean ignoreClientSign;

    public String getUserId() {
        return userId;
    }

    public UserAccountBean setUserId(String userId) {
        this.userId = userId;

        return this;
    }

    public String getPwd() {
        return pwd;
    }

    public UserAccountBean setPwd(String pwd) {
        this.pwd = pwd;

        return this;
    }

    public String getNewClientSign() {
        return newClientSign;
    }

    public UserAccountBean setNewClientSign(String newClientSign) {
        this.newClientSign = newClientSign;

        return this;
    }

    public String getOldClientSign() {
        return oldClientSign;
    }

    public UserAccountBean setOldClientSign(String oldClientSign) {
        this.oldClientSign = oldClientSign;

        return this;
    }

    public boolean isIgnoreClientSign() {
        return ignoreClientSign;
    }

    public UserAccountBean setIgnoreClientSign(boolean ignoreClientSign) {
        this.ignoreClientSign = ignoreClientSign;

        return this;
    }
}
