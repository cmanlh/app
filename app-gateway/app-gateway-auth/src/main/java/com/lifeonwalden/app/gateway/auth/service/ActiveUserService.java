/*
 *    Copyright 2019 CManLH
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.lifeonwalden.app.gateway.auth.service;

import com.lifeonwalden.app.gateway.auth.bean.ActiveUserBean;
import com.lifeonwalden.app.gateway.auth.bean.UserAccountBean;

public interface ActiveUserService {
    ActiveUserBean get(String sessionId);

    void clearLogoutUser(String sessionId);

    ActiveUserBean active(ActiveUserBean activeUser);

    void recordLogin(ActiveUserBean activeUser, boolean loginSuccess);

    void loginFailed(ActiveUserBean activeUser);

    boolean isAllowedLogin(String principal);

    UserAccountBean getAccount(String principal);
}
