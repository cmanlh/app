/*
 *    Copyright 2019 CManLH
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.lifeonwalden.app.gateway.auth.bean;

public class ActiveUserBean {
    private String sessionId;

    private String principal;

    private String ip;

    private String clientSign;

    public String getSessionId() {
        return sessionId;
    }

    public ActiveUserBean setSessionId(String sessionId) {
        this.sessionId = sessionId;

        return this;
    }

    public String getPrincipal() {
        return principal;
    }

    public ActiveUserBean setPrincipal(String principal) {
        this.principal = principal;

        return this;
    }

    public String getIp() {
        return ip;
    }

    public ActiveUserBean setIp(String ip) {
        this.ip = ip;

        return this;
    }

    public String getClientSign() {
        return clientSign;
    }

    public ActiveUserBean setClientSign(String clientSign) {
        this.clientSign = clientSign;

        return this;
    }
}
