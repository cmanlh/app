package com.lifeonwalden.app.gateway.auth.bean;

import java.io.Serializable;

public class UserLoginInfo implements Serializable {
    private static final long serialVersionUID = -7634805858410954855L;

    private String clientSign;

    private String userId;

    private String password;

    public String getClientSign() {
        return clientSign;
    }

    public UserLoginInfo setClientSign(String clientSign) {
        this.clientSign = clientSign;

        return this;
    }

    public String getUserId() {
        return userId;
    }

    public UserLoginInfo setUserId(String userId) {
        this.userId = userId;

        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserLoginInfo setPassword(String password) {
        this.password = password;

        return this;
    }
}
