package com.lifeonwalden.app.gateway.auth.bean;

import java.io.Serializable;

public class UserLoginBean implements Serializable {
    private static final long serialVersionUID = 6873523354210961776L;
    private String sign;

    public String getSign() {
        return sign;
    }

    public UserLoginBean setSign(String sign) {
        this.sign = sign;

        return this;
    }
}
