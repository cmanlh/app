package com.thirdparty.bean;

public class Normal {
    private int age;
    private Integer sex;

    public int getAge() {
        return age;
    }

    public Normal setAge(int age) {
        this.age = age;

        return this;
    }

    public Integer getSex() {
        return sex;
    }

    public Normal setSex(Integer sex) {
        this.sex = sex;

        return this;
    }
}
