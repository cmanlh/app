package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.FormatSchema;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.util.TokenBuffer;
import com.lifeonwalden.forestbatis.biz.bean.AbstractParamMapBean;

import java.io.IOException;

public class ClearObjectMapper extends ObjectMapper {
    public ClearObjectMapper() {
        super();
    }

    protected ClearObjectMapper(ObjectMapper src) {
        super(src);
    }

    @Override
    protected ObjectReader _newReader(DeserializationConfig config) {
        return new ClearObjectReader(this, config);
    }

    @Override
    protected ObjectReader _newReader(DeserializationConfig config, JavaType valueType, Object valueToUpdate, FormatSchema schema, InjectableValues injectableValues) {
        return new ClearObjectReader(this, config, valueType, valueToUpdate, schema, injectableValues);
    }

    @Override
    public ObjectMapper copy() {
        this._checkInvalidCopy(ClearObjectMapper.class);
        return new ClearObjectMapper(this);
    }

    @Override
    protected Object _readValue(DeserializationConfig cfg, JsonParser p, JavaType valueType) throws IOException {
        /* First: may need to read the next token, to initialize
         * state (either before first read from parser, or after
         * previous token has been cleared)
         */
        Object result;
        JsonToken t = _initForReading(p, valueType);
        final DeserializationContext ctxt = createDeserializationContext(p, cfg);
        if (t == JsonToken.VALUE_NULL) {
            // Ask JsonDeserializer what 'null value' to use:
            result = _findRootDeserializer(ctxt, valueType).getNullValue(ctxt);
        } else if (t == JsonToken.END_ARRAY || t == JsonToken.END_OBJECT) {
            result = null;
        } else { // pointing to event other than null
            JsonDeserializer<Object> deser = _findRootDeserializer(ctxt, valueType);
            // ok, let's get the value
            if (cfg.useRootWrapping()) {
                result = _unwrapAndDeserialize(p, ctxt, cfg, valueType, deser);
            } else {
                if (valueType.isTypeOrSubTypeOf(AbstractParamMapBean.class)) {
                    result = ((ParamMapBeanDeserializer) deser).deserialize(valueType, p, ctxt);
                } else if (valueType.isTypeOrSubTypeOf(com.lifeonwalden.forestbatis.bean.AbstractParamMapBean.class)) {
                    result = ((NewParamMapBeanDeserializer) deser).deserialize(valueType, p, ctxt);
                } else {
                    result = deser.deserialize(p, ctxt);
                }
            }
        }
        // Need to consume the token too
        p.clearCurrentToken();
        if (cfg.isEnabled(DeserializationFeature.FAIL_ON_TRAILING_TOKENS)) {
            _verifyNoTrailingTokens(p, ctxt, valueType);
        }
        return result;
    }

    @Override
    protected Object _readMapAndClose(JsonParser p0, JavaType valueType) throws IOException {
        try (JsonParser p = p0) {
            Object result;
            JsonToken t = _initForReading(p, valueType);
            final DeserializationConfig cfg = getDeserializationConfig();
            final DeserializationContext ctxt = createDeserializationContext(p, cfg);
            if (t == JsonToken.VALUE_NULL) {
                // Ask JsonDeserializer what 'null value' to use:
                result = _findRootDeserializer(ctxt, valueType).getNullValue(ctxt);
            } else if (t == JsonToken.END_ARRAY || t == JsonToken.END_OBJECT) {
                result = null;
            } else {
                JsonDeserializer<Object> deser = _findRootDeserializer(ctxt, valueType);
                if (cfg.useRootWrapping()) {
                    result = _unwrapAndDeserialize(p, ctxt, cfg, valueType, deser);
                } else {
                    if (valueType.isTypeOrSubTypeOf(AbstractParamMapBean.class)) {
                        result = ((ParamMapBeanDeserializer) deser).deserialize(valueType, p, ctxt);
                    } else if (valueType.isTypeOrSubTypeOf(com.lifeonwalden.forestbatis.bean.AbstractParamMapBean.class)) {
                        result = ((NewParamMapBeanDeserializer) deser).deserialize(valueType, p, ctxt);
                    } else {
                        result = deser.deserialize(p, ctxt);
                    }
                }
                ctxt.checkUnresolvedObjectId();
            }
            if (cfg.isEnabled(DeserializationFeature.FAIL_ON_TRAILING_TOKENS)) {
                _verifyNoTrailingTokens(p, ctxt, valueType);
            }
            return result;
        }
    }

    @Override
    protected Object _convert(Object fromValue, JavaType toValueType) throws IllegalArgumentException {
        // [databind#1433] Do not shortcut null values.
        // This defaults primitives and fires deserializer getNullValue hooks.
        if (fromValue != null) {
            // also, as per [databind#11], consider case for simple cast
            // But with caveats: one is that while everything is Object.class, we don't
            // want to "optimize" that out; and the other is that we also do not want
            // to lose conversions of generic types.
            Class<?> targetType = toValueType.getRawClass();
            if (targetType != Object.class
                    && !toValueType.hasGenericTypes()
                    && targetType.isAssignableFrom(fromValue.getClass())) {
                return fromValue;
            }
        }

        // Then use TokenBuffer, which is a JsonGenerator:
        TokenBuffer buf = new TokenBuffer(this, false);
        if (isEnabled(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)) {
            buf = buf.forceUseOfBigDecimal(true);
        }
        try {
            // inlined 'writeValue' with minor changes:
            // first: disable wrapping when writing
            SerializationConfig config = getSerializationConfig().without(SerializationFeature.WRAP_ROOT_VALUE);
            // no need to check for closing of TokenBuffer
            _serializerProvider(config).serializeValue(buf, fromValue);

            // then matching read, inlined 'readValue' with minor mods:
            final JsonParser p = buf.asParser();
            Object result;
            // ok to pass in existing feature flags; unwrapping handled by mapper
            final DeserializationConfig deserConfig = getDeserializationConfig();
            JsonToken t = _initForReading(p, toValueType);
            if (t == JsonToken.VALUE_NULL) {
                DeserializationContext ctxt = createDeserializationContext(p, deserConfig);
                result = _findRootDeserializer(ctxt, toValueType).getNullValue(ctxt);
            } else if (t == JsonToken.END_ARRAY || t == JsonToken.END_OBJECT) {
                result = null;
            } else { // pointing to event other than null
                DeserializationContext ctxt = createDeserializationContext(p, deserConfig);
                JsonDeserializer<Object> deser = _findRootDeserializer(ctxt, toValueType);
                // note: no handling of unwrapping
                if (toValueType.isTypeOrSubTypeOf(AbstractParamMapBean.class)) {
                    result = ((ParamMapBeanDeserializer) deser).deserialize(toValueType, p, ctxt);
                } else if (toValueType.isTypeOrSubTypeOf(com.lifeonwalden.forestbatis.bean.AbstractParamMapBean.class)) {
                    result = ((NewParamMapBeanDeserializer) deser).deserialize(toValueType, p, ctxt);
                } else {
                    result = deser.deserialize(p, ctxt);
                }
            }
            p.close();
            return result;
        } catch (IOException e) { // should not occur, no real i/o...
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    @Override
    protected JsonDeserializer<Object> _findRootDeserializer(DeserializationContext ctxt, JavaType valueType) throws JsonMappingException {
        // First: have we already seen it?
        JsonDeserializer<Object> deser = _rootDeserializers.get(valueType);
        if (deser != null) {
            return deser;
        }
        // Nope: need to ask provider to resolve it
        if (valueType.isTypeOrSubTypeOf(AbstractParamMapBean.class)) {
            MapType abstractParamMapType = getTypeFactory().constructMapType(AbstractParamMapBean.class, String.class, Object.class);
            deser = _rootDeserializers.get(abstractParamMapType);
            if (null == deser) {
                deser = new ParamMapBeanDeserializer();
            }
            _rootDeserializers.put(abstractParamMapType, deser);
        } else if (valueType.isTypeOrSubTypeOf(com.lifeonwalden.forestbatis.bean.AbstractParamMapBean.class)) {
            MapType abstractParamMapType = getTypeFactory().constructMapType(com.lifeonwalden.forestbatis.bean.AbstractParamMapBean.class, String.class, Object.class);
            deser = _rootDeserializers.get(abstractParamMapType);
            if (null == deser) {
                deser = new NewParamMapBeanDeserializer();
            }
            _rootDeserializers.put(abstractParamMapType, deser);
        } else {
            deser = ctxt.findRootValueDeserializer(valueType);
        }
        if (deser == null) { // can this happen?
            return ctxt.reportBadDefinition(valueType,
                    "Cannot find a deserializer for type " + valueType);
        }
        _rootDeserializers.put(valueType, deser);
        return deser;
    }
}