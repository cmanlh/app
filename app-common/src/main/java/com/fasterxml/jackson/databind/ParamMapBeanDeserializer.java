package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.lifeonwalden.app.util.logger.LoggerUtil;
import com.lifeonwalden.forestbatis.biz.bean.AbstractParamMapBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

public class ParamMapBeanDeserializer extends JsonDeserializer {
    private final static Logger logger = LoggerUtil.getLogger(ParamMapBeanDeserializer.class);

    @Override
    public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return ctxt.getContextualType();
    }

    public Object deserialize(JavaType valueType, JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        try {
            AbstractParamMapBean result = (AbstractParamMapBean) valueType._class.newInstance();

            String key;
            if (p.isExpectedStartObjectToken()) {
                key = p.nextFieldName();
            } else {
                JsonToken t = p.getCurrentToken();
                if (t == JsonToken.END_OBJECT) {
                    return result;
                }
                if (t != JsonToken.FIELD_NAME) {
                    ctxt.reportWrongTokenException(this, JsonToken.FIELD_NAME, null);
                }
                key = p.getCurrentName();
            }

            for (; key != null; key = p.nextFieldName()) {
                JsonToken t = p.nextToken();
                Object value = null;
                Class<?> clazz = result._getType(key);
                if (JsonToken.VALUE_NULL == t) {
                    continue;
                } else if (JsonToken.VALUE_STRING == t && (null == clazz || String.class.equals(clazz))) {
                    value = p.getValueAsString();
                } else {
                    switch (t) {
                        case VALUE_STRING: {
                            String strVal = p.getValueAsString();
                            if (StringUtils.isNotEmpty(strVal)) {
                                if (Integer.class.equals(clazz)) {
                                    value = Integer.parseInt(strVal);
                                } else if (BigDecimal.class.equals(clazz)) {
                                    value = new BigDecimal(strVal);
                                } else if (Boolean.class.equals(clazz)) {
                                    value = new Boolean(strVal);
                                } else if (Date.class.equals(clazz)) {
                                    value = new Date(Long.parseLong(strVal));
                                } else if (Long.class.equals(clazz)) {
                                    value = Long.parseLong(strVal);
                                } else {
                                    throw new RuntimeException("Invalid data format : " + key);
                                }
                            }
                            break;
                        }
                        case VALUE_NUMBER_INT: {
                            if (null == clazz) {
                                value = p.getIntValue();
                            } else {
                                if (clazz.equals(Long.class)) {
                                    value = p.getLongValue();
                                } else if (clazz.equals(Date.class)) {
                                    value = new Date(p.getLongValue());
                                } else if (clazz.equals(BigDecimal.class)) {
                                    value = BigDecimal.valueOf(p.getLongValue());
                                } else {
                                    value = p.getIntValue();
                                }
                            }
                            break;
                        }
                        case VALUE_NUMBER_FLOAT: {
                            if (null == clazz) {
                                value = p.getFloatValue();
                            } else {
                                if (clazz.equals(BigDecimal.class)) {
                                    value = p.getDecimalValue();
                                } else if (clazz.equals(Double.class)) {
                                    value = p.getDoubleValue();
                                } else {
                                    value = p.getFloatValue();
                                }
                            }
                            break;
                        }
                        case VALUE_TRUE:
                        case VALUE_FALSE:
                            value = p.getBooleanValue();
                            break;
                        case START_ARRAY: {
                            if (null == clazz) {
                                throw new RuntimeException("Must provide class of property for none string type when using subclass instance of AbstractParamMapBean. The property is ".concat(key));
                            }
                            value = p.readValueAs(new MapBeanTypeReference(clazz));
                            break;
                        }
                        case START_OBJECT: {
                            if (null == clazz) {
                                throw new RuntimeException("Must provide class of property for none string type when using subclass instance of AbstractParamMapBean. The property is ".concat(key));
                            }
                            value = p.readValueAs(result._getType(key));
                            break;
                        }
                    }
                }
                if (null != value) {
                    result.put(key, value);
                }
            }
            return result;
        } catch (InstantiationException e) {
            logger.error("Failed to instance for {}", valueType._class.getName());
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            logger.error("Failed to instance for {}", valueType._class.getName());

            throw new RuntimeException(e);
        }
    }
}
