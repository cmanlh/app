package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.type.TypeReference;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public class MapBeanTypeReference extends TypeReference<List> {
    protected final Type _type;

    public MapBeanTypeReference(Class<?> clazz) {
        _type = new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                return new Type[]{clazz};
            }

            @Override
            public Type getRawType() {
                return List.class;
            }

            @Override
            public Type getOwnerType() {
                return null;
            }

            @Override
            public boolean equals(Object obj) {
                if (null == obj) {
                    return false;
                }

                if (obj instanceof ParameterizedType) {
                    ParameterizedType parameterizedType = (ParameterizedType) obj;

                    if (parameterizedType.getRawType().equals(List.class) && parameterizedType.getActualTypeArguments().length == 1 && parameterizedType.getActualTypeArguments()[0].equals(clazz)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        };
    }

    @Override
    public Type getType() {
        return this._type;
    }

    @Override
    public int compareTo(TypeReference o) {
        if (null == o) {
            return -1;
        }
        if (o.getType().equals(_type)) {
            return 0;
        } else {
            return -1;
        }
    }
}
