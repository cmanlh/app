package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.filter.JsonPointerBasedFilter;
import com.fasterxml.jackson.core.filter.TokenFilter;
import com.fasterxml.jackson.databind.deser.DataFormatReaders;
import com.fasterxml.jackson.databind.type.MapType;
import com.lifeonwalden.forestbatis.biz.bean.AbstractParamMapBean;

import java.io.IOException;

public class ClearObjectReader extends ObjectReader {
    protected ClearObjectReader(ObjectMapper mapper, DeserializationConfig config) {
        super(mapper, config);
    }

    protected ClearObjectReader(ObjectMapper mapper, DeserializationConfig config, JavaType valueType, Object valueToUpdate, FormatSchema schema, InjectableValues injectableValues) {
        super(mapper, config, valueType, valueToUpdate, schema, injectableValues);
    }

    protected ClearObjectReader(ObjectReader base, DeserializationConfig config, JavaType valueType, JsonDeserializer<Object> rootDeser, Object valueToUpdate, FormatSchema schema, InjectableValues injectableValues, DataFormatReaders dataFormatReaders) {
        super(base, config, valueType, rootDeser, valueToUpdate, schema, injectableValues, dataFormatReaders);
    }

    protected ClearObjectReader(ObjectReader base, DeserializationConfig config) {
        super(base, config);
    }

    protected ClearObjectReader(ObjectReader base, JsonFactory f) {
        super(base, f);
    }

    protected ClearObjectReader(ObjectReader base, TokenFilter filter) {
        super(base, filter);
    }

    @Override
    protected ObjectReader _new(ObjectReader base, JsonFactory f) {
        return new ClearObjectReader(base, f);
    }

    @Override
    protected ObjectReader _new(ObjectReader base, DeserializationConfig config) {
        return new ClearObjectReader(base, config);
    }

    @Override
    protected ObjectReader _new(ObjectReader base, DeserializationConfig config, JavaType valueType, JsonDeserializer<Object> rootDeser, Object valueToUpdate, FormatSchema schema, InjectableValues injectableValues, DataFormatReaders dataFormatReaders) {
        return new ClearObjectReader(base, config, valueType, rootDeser, valueToUpdate, schema, injectableValues, dataFormatReaders);
    }

    @Override
    public ObjectReader at(String value) {
        return new ClearObjectReader(this, new JsonPointerBasedFilter(value));
    }

    @Override
    public ObjectReader at(JsonPointer pointer) {
        return new ClearObjectReader(this, new JsonPointerBasedFilter(pointer));
    }

    @Override
    protected JsonDeserializer<Object> _prefetchRootDeserializer(JavaType valueType) {
        if ((valueType == null) || !_config.isEnabled(DeserializationFeature.EAGER_DESERIALIZER_FETCH)) {
            return null;
        }
        // already cached?
        JsonDeserializer<Object> deser = _rootDeserializers.get(valueType);
        if (deser == null) {
            try {
                // If not, need to resolve; for which we need a temporary context as well:
                if (valueType.isTypeOrSubTypeOf(AbstractParamMapBean.class)) {
                    MapType abstractParamMapType = getTypeFactory().constructMapType(AbstractParamMapBean.class, String.class, Object.class);
                    deser = _rootDeserializers.get(abstractParamMapType);
                    if (null == deser) {
                        deser = new ParamMapBeanDeserializer();
                    }
                    _rootDeserializers.put(abstractParamMapType, deser);
                } else if (valueType.isTypeOrSubTypeOf(com.lifeonwalden.forestbatis.bean.AbstractParamMapBean.class)) {
                    MapType abstractParamMapType = getTypeFactory().constructMapType(com.lifeonwalden.forestbatis.bean.AbstractParamMapBean.class, String.class, Object.class);
                    deser = _rootDeserializers.get(abstractParamMapType);
                    if (null == deser) {
                        deser = new NewParamMapBeanDeserializer();
                    }
                    _rootDeserializers.put(abstractParamMapType, deser);
                } else {
                    DeserializationContext ctxt = createDeserializationContext(null);
                    deser = ctxt.findRootValueDeserializer(valueType);
                }
                if (deser != null) {
                    _rootDeserializers.put(valueType, deser);
                }
                return deser;
            } catch (JsonProcessingException e) {
                // need to swallow?
            }
        }
        return deser;
    }

    @Override
    protected Object _bindAndClose(JsonParser p0) throws IOException {
        try (JsonParser p = p0) {
            Object result;

            DeserializationContext ctxt = createDeserializationContext(p);
            JsonToken t = _initForReading(ctxt, p);
            if (t == JsonToken.VALUE_NULL) {
                if (_valueToUpdate == null) {
                    result = _findRootDeserializer(ctxt).getNullValue(ctxt);
                } else {
                    result = _valueToUpdate;
                }
            } else if (t == JsonToken.END_ARRAY || t == JsonToken.END_OBJECT) {
                result = _valueToUpdate;
            } else {
                JsonDeserializer<Object> deser = _findRootDeserializer(ctxt);
                if (_unwrapRoot) {
                    result = _unwrapAndDeserialize(p, ctxt, _valueType, deser);
                } else {
                    if (_valueType.isTypeOrSubTypeOf(AbstractParamMapBean.class)) {
                        return ((ParamMapBeanDeserializer) deser).deserialize(_valueType, p, ctxt);
                    } else if (_valueType.isTypeOrSubTypeOf(com.lifeonwalden.forestbatis.bean.AbstractParamMapBean.class)) {
                        return ((NewParamMapBeanDeserializer) deser).deserialize(_valueType, p, ctxt);
                    } else if (_valueToUpdate == null) {
                        result = deser.deserialize(p, ctxt);
                    } else {
                        deser.deserialize(p, ctxt, _valueToUpdate);
                        result = _valueToUpdate;
                    }
                }
            }
            if (_config.isEnabled(DeserializationFeature.FAIL_ON_TRAILING_TOKENS)) {
                _verifyNoTrailingTokens(p, ctxt, _valueType);
            }
            return result;
        }
    }
}
